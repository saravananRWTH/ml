//
// Created by palanisamy on 25.09.20.
//

// COPYRIGHT 2020 Saravanan Palanisamy

#include <boost/test/included/unit_test.hpp>
// #include "stats/Transpose/Transpose.h"
#include "matrix/transpose/Transpose.h"

using namespace ML;
BOOST_AUTO_TEST_SUITE(unit_tests)
BOOST_AUTO_TEST_SUITE(Transpose_test)

BOOST_AUTO_TEST_CASE(tranpose_matrix) {
  Transpose transpose;
  std::vector<std::vector<float>> points1{{1, 2}, {4, 5}, {6, 7}, {8, 9}};
  transpose.calculate_transpose(points1);
}

BOOST_AUTO_TEST_CASE(calculate_matrix_transpose) {
  Transpose transpose;
// auto kernel = ocl.create("library/vector_add.cl", "vector_add");
  std::vector<std::vector<float>> points1 = {{1, 2}, {4, 5}, {6, 7}};
  std::vector<std::vector<float>> points2 = {{3, 5}, {6, 7}, {8, 9}};
  auto distance = transpose.calculate_transpose(points1);
  BOOST_CHECK(distance);
  for (auto i:distance.value())
    cout << i << endl;
}



BOOST_AUTO_TEST_SUITE_END();
BOOST_AUTO_TEST_SUITE_END();
