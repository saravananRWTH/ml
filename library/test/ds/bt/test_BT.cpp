//
// Created by palanisamy on 05.11.20.
//

// COPYRIGHT 2020 Saravanan Palanisamy

//
// Created by palanisamy on 04.11.20.
//

// COPYRIGHT 2020 Saravanan Palanisamy

#include <boost/test/included/unit_test.hpp>
// #include "stats/Transpose/Transpose.h"
#include "ds/bt/bt.h"
using namespace ds;
BOOST_AUTO_TEST_SUITE(unit_tests)
BOOST_AUTO_TEST_SUITE(bt_test)

BOOST_AUTO_TEST_CASE(create_simple_bt) {
  Binary_Tree<int> bt;
  bt.create(10);
  bt.create(20);
  bt.create(30);
  bt.insert(40);
  int temp = 50;
  for (int i = 1; i < 10; i++) {
    bt.insert(temp);
    temp += 10;
  }
  //  bt.create(40);
  std::cout << "IN ORDER ";
  bt.print(IN_ORDER);
  std::cout << "POST ORDER ";
  bt.print(POST_ORDER);
  std::cout << "PRE ORDER ";
  bt.print(PRE_ORDER);
}

BOOST_AUTO_TEST_CASE(check_fullness) {
  Binary_Tree<int> bt;
  BOOST_CHECK(bt.is_full());
  bt.create(1);
  BOOST_CHECK(bt.is_full());
  bt.insert(2);
  BOOST_CHECK(!bt.is_full());
  bt.insert(3);
  BOOST_CHECK(bt.is_full());
  bt.insert(4);
  BOOST_CHECK(!bt.is_full());
  bt.insert(5);
  BOOST_CHECK(bt.is_full());
  bt.insert(6);
  BOOST_CHECK(!bt.is_full());
  bt.insert(7);
  BOOST_CHECK(bt.is_full());
}

BOOST_AUTO_TEST_CASE(check_perfect) {
  Binary_Tree<int> bt;
  BOOST_CHECK(bt.is_perfect());
  bt.create(1);
  BOOST_CHECK(bt.is_perfect());
  bt.insert(2);
  BOOST_CHECK(!bt.is_perfect());
  bt.insert(3);
  BOOST_CHECK(bt.is_perfect());
  bt.insert(4);
  BOOST_CHECK(!bt.is_perfect());
  bt.insert(5);
  BOOST_CHECK(!bt.is_perfect());
  bt.insert(6);
  BOOST_CHECK(!bt.is_perfect());
}

BOOST_AUTO_TEST_CASE(check_complete) {
  std::vector<int> a = {1, 12, 9, 5, 6, 10};
  Binary_Tree<int> bt;
  bt.create_complete(a);
  bt.print(IN_ORDER);
  BOOST_CHECK(bt.is_complete());
}

BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()
