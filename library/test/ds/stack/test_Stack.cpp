//
// Created by palanisamy on 04.11.20.
//

// COPYRIGHT 2020 Saravanan Palanisamy
#include <boost/test/included/unit_test.hpp>
// #include "stats/Transpose/Transpose.h"
#include "ds/stack/stack.h"
using namespace ds;
BOOST_AUTO_TEST_SUITE(unit_tests)
BOOST_AUTO_TEST_SUITE(Stack_test)

BOOST_AUTO_TEST_CASE(create_stack) {
  Stack<int> r;
  r.create(20, 12);
  r.push(30);

  int temp = 40;

  for (int i = 1; i < 10; i++) {
    r.push(temp);
    temp += 10;
  }

  r.print();
  r.pop();
  r.print();

  r.peek();
  r.clean();
  r.print();
}

BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()
