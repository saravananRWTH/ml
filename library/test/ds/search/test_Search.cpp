//
// Created by palanisamy on 07.11.20.
//

// COPYRIGHT 2020 Saravanan Palanisamy

#include <boost/test/included/unit_test.hpp>
// #include "stats/Transpose/Transpose.h"
#include "ds/search/search.h"
using namespace ds;
BOOST_AUTO_TEST_SUITE(unit_tests)
BOOST_AUTO_TEST_SUITE(Search_test)

BOOST_AUTO_TEST_CASE(search) {
  Search<int> search(BINARY_SEARCH);
  std::vector<int> data{42, 32, 33, 52, 37, 47, 51};
  BOOST_CHECK(search.search(data, 51));
  BOOST_CHECK(search.search(data, 52));
  BOOST_CHECK(search.search(data, 42));
  BOOST_CHECK(search.search(data, 32));
  BOOST_CHECK(search.search(data, 33));
  BOOST_CHECK(search.search(data, 37));
  BOOST_CHECK(search.search(data, 47));
  BOOST_CHECK(!search.search(data, 48));
}

BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()
