//
// Created by palanisamy on 16.10.20.
//

// COPYRIGHT 2020 Saravanan Palanisamy
#include <boost/test/included/unit_test.hpp>
// #include "stats/Transpose/Transpose.h"
#include "ds/bst/bst.h"

using namespace ds;
BOOST_AUTO_TEST_SUITE(unit_tests)
BOOST_AUTO_TEST_SUITE(BST_test)
BOOST_AUTO_TEST_CASE(add_first_value_to_bst) {
  BST<int> bst;
  bst.insert(100);
  bst.insert(40);
  int temp = 50;
  for (int i = 1; i < 10; i++) {
    bst.insert(temp);
    temp += 10;
  }

  //  bt.create(40);
  std::cout << "IN ORDER ";
  bst.print(bst.IN_ORDER);
  std::cout << "POST ORDER ";
  bst.print(bst.POST_ORDER);
  std::cout << "PRE ORDER ";
  bst.print(bst.PRE_ORDER);

  bst.search(50);
  bst.search(140);
  bst.remove(130);
  //  bt.create(40);
  std::cout << "IN ORDER ";
  bst.print(bst.IN_ORDER);
  std::cout << "POST ORDER ";
  bst.print(bst.POST_ORDER);
  std::cout << "PRE ORDER ";
  bst.print(bst.PRE_ORDER);

  bst.remove(100);
  //  bt.create(40);
  std::cout << "IN ORDER ";
  bst.print(bst.IN_ORDER);
  std::cout << "POST ORDER ";
  bst.print(bst.POST_ORDER);
  std::cout << "PRE ORDER ";
  bst.print(bst.PRE_ORDER);

}
BOOST_AUTO_TEST_SUITE_END();
BOOST_AUTO_TEST_SUITE_END();
