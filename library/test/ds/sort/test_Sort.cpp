//
// Created by palanisamy on 05.11.20.
//

// COPYRIGHT 2020 Saravanan Palanisamy

#include <boost/test/included/unit_test.hpp>
// #include "stats/Transpose/Transpose.h"
#include "ds/bubble_sort/bubble_sort.h"
using namespace algorithm;
BOOST_AUTO_TEST_SUITE(unit_tests)
BOOST_AUTO_TEST_SUITE(Sort_test)

BOOST_AUTO_TEST_CASE(bubble_sort) {
  Sort<int> sort(BUBBLE_SORT);
  std::vector<int> data{-2, 45, 0, 11, -9};
  std::vector<int> data1{22, 91, 35, 78, 10, 8, 75, 99, 1, 67};
  std::vector<int> data2{9, 5, 1, 4, 3};
  cout << "\n";
  cout << "BUBBLE SORT " << "\n";
  cout << "\n";
  sort.sort(data);
  sort.print(data);
  sort.sort(data1);
  sort.print(data1);
  sort.sort(data2);
  sort.print(data2);
  data = {-2, 45, 0, 11, -9};
  data1 = {22, 91, 35, 78, 10, 8, 75, 99, 1, 67};
  data2 = {9, 5, 1, 4, 3};

  cout << "\n";
  cout << "SELECTION SORT " << "\n";
  cout << "\n";

  sort.set_type(SELECTION_SORT);
  sort.sort(data);
  sort.print(data);
  sort.sort(data1);
  sort.print(data1);
  sort.sort(data2);
  sort.print(data2);

  data = {-2, 45, 0, 11, -9};
  data1 = {22, 91, 35, 78, 10, 8, 75, 99, 1, 67};
  data2 = {9, 5, 1, 4, 3};

  cout << "\n";
  cout << "INSERTION SORT " << "\n";
  cout << "\n";

  sort.set_type(INSERTION_SORT);
  sort.sort(data);
  sort.print(data);
  sort.sort(data1);
  sort.print(data1);
  sort.sort(data2);
  sort.print(data2);

  data = {-2, 45, 0, 11, -9};
  data1 = {22, 91, 35, 78, 10, 8, 75, 99, 1, 67};
  data2 = {9, 5, 1, 4, 3};

  cout << "\n";
  cout << "MERGE SORT " << "\n";
  cout << "\n";

  sort.set_type(MERGE_SORT);
  sort.sort(data);
  sort.print(data);
  sort.sort(data1);
  sort.print(data1);
  sort.sort(data2);
  sort.print(data2);

}

BOOST_AUTO_TEST_CASE(MERGE_SORT_TEST) {
  std::vector<int> data{-2, 45, 0, 11, -9};
  std::vector<int> data1{22, 91, 35, 78, 10, 8, 75, 99, 1, 67};
  std::vector<int> data2{9, 5, 1, 4, 3};
  cout << "\n";
  cout << "MERGE SORT " << "\n";
  cout << "\n";
  Sort<int> sort(MERGE_SORT);
  sort.sort(data);
  sort.print(data);
  sort.sort(data1);
  sort.print(data1);
  sort.sort(data2);
  sort.print(data2);
}

BOOST_AUTO_TEST_CASE(QUICK_SORT_TEST) {
  std::vector<int> data{-2, 45, 0, 11, -9};
  std::vector<int> data1{22, 91, 35, 78, 10, 8, 75, 99, 1, 67};
  std::vector<int> data2{9, 5, 1, 4, 3};
  std::vector<int> data3{8, 7, 6, 1, 0, 9, 2};
  cout << "\n";
  cout << "QUICK SORT " << "\n";
  cout << "\n";
  Sort<int> sort(QUICK_SORT);
  sort.sort(data);
  sort.print(data);
  sort.sort(data1);
  sort.print(data1);
  sort.sort(data2);
  sort.print(data2);
  sort.sort(data3);
  sort.print(data3);
}

BOOST_AUTO_TEST_CASE(COUNT_SORT_TEST) {
  std::vector<int> data{4, 2, 2, 8, 3, 3, 1};
  cout << "\n";
  cout << "COUNT SORT " << "\n";
  cout << "\n";
  Sort<int> sort(COUNT_SORT);
  sort.sort(data);
  sort.print(data);
}

BOOST_AUTO_TEST_CASE(RADIX_SORT_TEST) {
  std::vector<int> data{121, 432, 564, 23, 1, 45, 788};
  cout << "\n";
  cout << "RADIX SORT " << "\n";
  cout << "\n";
  Sort<int> sort(RADIX_SORT);
  sort.sort(data);
  sort.print(data);
}

BOOST_AUTO_TEST_CASE(BUCKET_SORT_TEST) {
  std::vector<int> data{42, 32, 33, 52, 37, 47, 51};
  cout << "\n";
  cout << "BUCKET SORT " << "\n";
  cout << "\n";
  Sort<int> sort(BUCKET_SORT);
  sort.sort(data);
}

BOOST_AUTO_TEST_CASE(HEAP_SORT_TEST) {
  std::vector<int> data{42, 32, 33, 52, 37, 47, 51};
  std::vector<int> data1{22, 91, 35, 78, 10, 8, 75, 99, 1, 67};
  std::vector<int> data2{9, 5, 1, 4, 3};
  std::vector<int> data3{8, 7, 6, 1, 0, 9, 2};
  std::vector<int> data4{-2, 45, 0, 11, -9};

  cout << "\n";
  cout << "BUCKET SORT " << "\n";
  cout << "\n";
  Sort<int> sort(HEAP_SORT);
  sort.sort(data);
  sort.print(data);
  sort.sort(data1);
  sort.print(data1);
  sort.sort(data2);
  sort.print(data2);
  sort.sort(data3);
  sort.print(data3);
  sort.sort(data4);
  sort.print(data4);
}

BOOST_AUTO_TEST_CASE(SHELL_SORT_TEST) {
  std::vector<int> data{42, 32, 33, 52, 37, 47, 51};
  std::vector<int> data1{9, 8, 3, 7, 5, 6, 4, 1};
  std::vector<int> data2{9, 5, 1, 4, 3};
  std::vector<int> data3{8, 7, 6, 1, 0, 9, 2};
  std::vector<int> data4{-2, 45, 0, 11, -9};
  std::vector<int> data5{22, 91, 35, 78, 10, 8, 75, 99, 1, 67};

  cout << "\n";
  cout << "SHELL  SORT " << "\n";
  cout << "\n";
  Sort<int> sort(SHELL_SORT);
  sort.sort(data1);
  sort.print(data1);
  sort.sort(data);
  sort.print(data);
  sort.sort(data2);
  sort.print(data2);
  sort.sort(data3);
  sort.print(data3);
  sort.sort(data4);
  sort.print(data4);
  sort.sort(data5);
  sort.print(data5);

}

BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()

