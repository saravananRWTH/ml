//
// Created by palanisamy on 04.11.20.
//

// COPYRIGHT 2020 Saravanan Palanisamy

#include <boost/test/included/unit_test.hpp>
// #include "stats/Transpose/Transpose.h"
#include "ds/queue/queue.h"
using namespace ds;
BOOST_AUTO_TEST_SUITE(unit_tests)
BOOST_AUTO_TEST_SUITE(Queue_test)

BOOST_AUTO_TEST_CASE(create_simple_queue) {
  Queue<int> queue;
  queue.front_enqueue(10);
  queue.front_enqueue(20);
  queue.front_enqueue(30);
  queue.print();

  queue.front_dequeue();
  queue.print();
}

BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()
