//
// Created by palanisamy on 06.11.20.
//

// COPYRIGHT 2020 Saravanan Palanisamy
//
// Created by palanisamy on 05.11.20.
//

// COPYRIGHT 2020 Saravanan Palanisamy

//
// Created by palanisamy on 04.11.20.
//

// COPYRIGHT 2020 Saravanan Palanisamy

#include <boost/test/included/unit_test.hpp>
// #include "stats/Transpose/Transpose.h"
#include "ds/bt/bt.h"
#include "ds/heap/heap.h"
using namespace ds;
BOOST_AUTO_TEST_SUITE(unit_tests)
BOOST_AUTO_TEST_SUITE(heap_test)

BOOST_AUTO_TEST_CASE(create_heap) {
  std::vector<int> data{3, 9, 2, 1, 4, 5};
  Heap<int> heap;
  heap.create_complete_bt(data);
  heap.print(IN_ORDER);
  BOOST_CHECK(heap.is_complete());
  BOOST_CHECK(heap.totalNumberOfNodes() == data.size());
  heap.heapify();
  heap.print(IN_ORDER);
  heap.print(PRE_ORDER);
  heap.print(POST_ORDER);

  Heap<int> heap1;

  heap.insert_node(7);
  heap.print(IN_ORDER);

  heap.delete_node(3);
  cout << "after deleting " << " \n";
  heap.print(IN_ORDER);

  heap.insert_node(16);
  heap.insert_node(3);
  heap.print(IN_ORDER);

  BOOST_CHECK(heap.maximum_value() == 16);

  std::vector<int> data1;
  std::vector<int> data2{3, 9, 2, 1, 4, 5, 8, 10, 6, 7};
  heap1.create_complete_bt(data2);
  heap1.print(IN_ORDER);

  for (int i = 0; i < 10; i++) {
    int temp = heap.extract_maximum();
    data1.push_back(temp);
  }

  cout << "sorted heap : \n";
  for (auto i:data1) {
    cout << i << " ";
  }
  cout << " \n";


  /*  heap.insert_node(10);
  heap.print(IN_ORDER);
  heap.insert_node(0);
  heap.print(IN_ORDER);
  heap.insert_node(11);
  heap.print(IN_ORDER);*/


}

BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()
