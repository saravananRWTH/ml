//
// Created by palanisamy on 11.09.20.
//

// COPYRIGHT 2020 Saravanan Palanisamy


#include "stats/lpNorm/LpNorm.h"
// COPYRIGHT 2020 Saravanan Palanisamy

#include <boost/test/included/unit_test.hpp>
#include "opencl/Opencl.h"

using namespace ML;
BOOST_AUTO_TEST_SUITE(unit_tests)
BOOST_AUTO_TEST_SUITE(LpNorm_test)

BOOST_AUTO_TEST_CASE(LpNorm_initiialize) {
  LpNorm lpNorm;
// auto kernel = ocl.create("library/vector_add.cl", "vector_add");
  auto queue = lpNorm.createCommandQueue(0, 0);
  // auto kernel = lpNorm.createProgramKernel("LpNorm.cl", "LpNorm");
  std::vector<std::vector<float>> points1 = {{1, 2}, {4, 5}, {6, 7}};
  float lp_[points1.size()];
  auto points2 = points1;
  auto lp = lpNorm.calculate_lp_norm(points2, 2);

  BOOST_CHECK(lp);

  for (auto i:lp.value())
    cout << "the lp " << i << endl;
}

BOOST_AUTO_TEST_SUITE_END();
BOOST_AUTO_TEST_SUITE_END();
