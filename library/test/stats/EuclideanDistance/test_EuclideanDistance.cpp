//
// Created by palanisamy on 23.09.20.
//

// COPYRIGHT 2020 Saravanan Palanisamy

#include <boost/test/included/unit_test.hpp>
// #include "stats/EuclideanDistance/EuclideanDistance.h"
#include "stats/euclidean_distance/EuclideanDistance.h"

using namespace ML;
BOOST_AUTO_TEST_SUITE(unit_tests)
BOOST_AUTO_TEST_SUITE(EuclideanDistance_test)

BOOST_AUTO_TEST_CASE(convert_2d_to_1d_) {
  std::vector<std::vector<float>> points1{{1, 2}, {4, 5}, {6, 7}};
  float *v = convert_2d_to_1d(points1);
  for (int i = 0; i < points1.size() * points1[0].size(); i++) {
    cout << v[i] << endl;
  }

}

BOOST_AUTO_TEST_CASE(calculate_euclidean_norm) {
  EuclideanDistance euclideanDistance;
// auto kernel = ocl.create("library/vector_add.cl", "vector_add");
  auto queue = euclideanDistance.createCommandQueue(0, 0);
  auto result = euclideanDistance.createProgramKernel("EuclideanDistance.cl", "EuclideanDistance");
  BOOST_CHECK(result);
  std::vector<std::vector<float>> points1 = {{1, 2}, {4, 5}, {6, 7}};
  std::vector<std::vector<float>> points2 = {{3, 5}, {6, 7}, {8, 9}};
  auto distance = euclideanDistance.calculate_euclidean_distance(points1, points2);
  BOOST_CHECK(distance);
  for (auto i:distance.value())
    cout << i << endl;

}

BOOST_AUTO_TEST_SUITE_END();
BOOST_AUTO_TEST_SUITE_END();
