//
// Created by palanisamy on 13.09.20.
//

// COPYRIGHT 2020 Saravanan Palanisamy

#include <boost/test/included/unit_test.hpp>
#include "opencl/Opencl.h"

using namespace ML;
BOOST_AUTO_TEST_SUITE(unit_tests)
BOOST_AUTO_TEST_SUITE(OpenCL_test)

BOOST_AUTO_TEST_CASE(opencl_initiialize) {
  ML::OpenCLFactory ocl;
// auto kernel = ocl.create("library/vector_add.cl", "vector_add");
  ocl.setHostProgram();
  auto queue = ocl.createCommandQueue(0, 0);
  BOOST_CHECK(queue);
  auto kernel = ocl.createProgramKernel("vector_add.cl", "vector_add");
  BOOST_CHECK(kernel);
}

BOOST_AUTO_TEST_SUITE_END();
BOOST_AUTO_TEST_SUITE_END();

