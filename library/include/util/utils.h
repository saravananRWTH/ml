//
// Created by palanisamy on 22.09.20.
//

// COPYRIGHT 2020 Saravanan Palanisamy
#ifndef MLTEMPLATE_LIBRARY_INCLUDE_UTILS_H_
#define MLTEMPLATE_LIBRARY_INCLUDE_UTILS_H_

#pragma once

#include <typeinfo>
#include <algorithm>
#include <utility>
#include <array>
#include <string>
#include <map>

#include "tl/expected.hpp"
using namespace std;
namespace ML {
template<class T> using MLResult =
tl::expected<T, std::map<std::string, std::string>>;

template<class T>
MLResult<T> MLError(std::string current_function,
                    int line, std::string e) {
  return tl::make_unexpected(std::map<std::string, std::string>{
      std::pair<std::string, std::string>(current_function +
          std::string(": Line ") + std::to_string(line) +
          std::string(": "), e)});
}

template<class T>
void print_error(MLResult<T> error) {
  if (!error.has_value()) {
    for (const auto &[key, value] : error.error()) {
      std::cout << key << ": " << value << "'\n";
    }
  }
}

void err_check(int err, std::string err_code) {
  if (err != CL_SUCCESS) {
    cout << "Error: " << err_code << "(" << err << ")" << endl;
    exit(-1);
  }
}

template<class T>
T *convert_2d_to_1d(std::vector<std::vector<T>> &points) {
  T *p = new T[points.size() * points[0].size()];
  try {
    auto *ptrArray = p;
    for (int i = 0; i < points.size(); i++) {
      std::copy(points[i].begin(), points[i].end(), ptrArray);
      ptrArray += points[i].size();
    }

  } catch (std::exception &e) {
    throw ("Error in converting 2d array to 1d array");
  }
  return p;
}

// Round Up Division function
size_t shrRoundUp(int group_size, int global_size)
{
  int r = global_size % group_size;
  if(r == 0)
  {
    return global_size;
  } else
  {
    return global_size + group_size - r;
  }
}

}  // namespace ML

#endif //MLTEMPLATE_LIBRARY_INCLUDE_UTILS_H_
