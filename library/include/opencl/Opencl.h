//
// Created by palanisamy on 13.09.20.
//

// COPYRIGHT 2020 Saravanan Palanisamy
#ifndef MLTEMPLATE_LIBRARY_INCLUDE_OPENCL_TEST_OPENCL_H_
#define MLTEMPLATE_LIBRARY_INCLUDE_OPENCL_OPENCL_H_
#define __CL_ENABLE_EXCEPTIONS
#pragma once

#include <iostream>
#include <ctime>
#ifdef __APPLE__
#include <OpenCL/cl.hpp>
#else
#include <CL/cl.hpp>
#endif
#include <filesystem>
#ifdef WINDOWS
#include <direct.h>
#define GetCurrentDir _getcwd
#else
#include <unistd.h>
#define GetCurrentDir getcwd
#endif
#include <fstream>
#include <tuple>
#include <boost/current_function.hpp>

#include "../util/utils.h"

using namespace std;
using namespace cl;

namespace ML {
class OpenCLFactory {
 public:
  int create(string file_path, const char *function_name);
  std::vector<cl::Platform> platformList;
  std::vector<cl::Device> devices;
  cl::Device default_device;
  cl::Program program;
  cl::Kernel kernel;
  cl::Context context;
  cl::CommandQueue queue;
  std::vector<cl::CommandQueue> queueList;
  std::vector<cl::Kernel> kernelList;

  MLResult<int> createCommandQueueList(int properties = CL_QUEUE_PROFILING_ENABLE) {
    MLResult<int> result = 0;
    try {
      for (auto i:devices) {
        cl::CommandQueue queue1(context, i, properties);
        queueList.push_back(std::move(queue1));
      }
    }
    catch (cl::Error &e) {
      result = MLError<int>(BOOST_CURRENT_FUNCTION, __LINE__,
                            std::string(" Get Context  error: ") + std::string(e.what()) + " "
                                + std::string(reinterpret_cast<const char *>(e.err())));
    }
    return result;
  }

  MLResult<int>
  createCommandQueue(int device_pos = 0, int properties = 0) {
    MLResult<int> result = 0;
    try {
      cl::CommandQueue queue1(context, devices[device_pos], properties);
      queue = std::move(queue1);
    }
    catch (cl::Error &e) {
      result = MLError<int>(BOOST_CURRENT_FUNCTION, __LINE__,
                            std::string(" Get Context  error: ") + std::string(e.what()) + " "
                                + std::string(reinterpret_cast<const char *>(e.err())));
    }
    return result;
  }

  MLResult<int> setHostProgram() {
    // creating command queue
    MLResult<int> result = 0;
    try {
      result = getDefaultPlatform()
          .and_then([&](const MLResult<int> &) {
            return getDevices();
          }).and_then([&](const MLResult<int> &) {
        return getContext();
      });
    }
    catch (cl::Error &e) {
      result = MLError<int>(BOOST_CURRENT_FUNCTION, __LINE__,
                            std::string(" Get Context  error: ") + std::string(e.what()) + " "
                                + std::string(reinterpret_cast<const char *>(e.err())));
    }
    return result;
  }

  MLResult<int> createProgramKernel(string file_path = "",
                                    char const *function_name = "") {
    MLResult<int> result = 0;
    try {
      std::ifstream sourceFile(file_path);
      std::string sourceCode(
          std::istreambuf_iterator<char>(sourceFile),
          (std::istreambuf_iterator<char>()));

      cl::Program::Sources source(1, std::make_pair(sourceCode.c_str(), sourceCode.length() + 1));
      cl::Program program = cl::Program(context, source);
      if (program.build({devices}) != CL_SUCCESS) {
        std::cout << " Error building: " << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(default_device) << "\n";
        exit(1);
      }
      err = program.build(devices);
      cl::Kernel kernel1(program, function_name);
      kernel = std::move(kernel1);
    } catch (cl::Error &e) {
      result = MLError<int>(BOOST_CURRENT_FUNCTION, __LINE__,
                            std::string(" Get Context  error: ") + std::string(e.what()) + " "
                                + std::string(reinterpret_cast<const char *>(e.err())));
    }
    return result;
  }

  int getDeviceCount() {
    return devices.size();
  }

  cl_int err;
 private:
  MLResult<int> getDefaultPlatform() {
    MLResult<int> result = 0;
    try {
      cl::Platform::get(&platformList);
      if (platformList.size() == 0) {
        std::cout << " No platforms found. Check OpenCL installation!\n";
        exit(1);
      }
      std::cout << "platform size " << platformList.size();
      for (auto i : platformList) {
        std::cout << "\n  platform list  " << i.getInfo<CL_PLATFORM_NAME>() << "\n";
      }

      std::cout << "\n  Using platform: " << platformList[0].getInfo<CL_PLATFORM_NAME>() << "\n";
    } catch (cl::Error &e) {
      result = MLError<int>(BOOST_CURRENT_FUNCTION, __LINE__,
                            std::string(" Get Default Platform  error: ") + std::string(e.what()) + " "
                                + std::string(reinterpret_cast<const char *>(e.err())));
    }
    return result;

    // return platformList[0];
  }

  MLResult<int> getContext() {
    MLResult<int> result = 0;
    try {
      cl::Context context1({default_device});
      context = std::move(context1);
    } catch (cl::Error &e) {
      result = MLError<int>(BOOST_CURRENT_FUNCTION, __LINE__,
                            std::string(" Get Context  error: ") + std::string(e.what()) + " "
                                + std::string(reinterpret_cast<const char *>(e.err())));
    }
    return result;
  }

  MLResult<int> getDevices() {
    MLResult<int> result = 0;
    try {
      platformList[0].getDevices(CL_DEVICE_TYPE_ALL, &devices);
      if (devices.size() == 0) {
        std::cout << " No devices found. Check OpenCL installation!\n";
        exit(1);
      }
      // use device[1] because that's a GPU; device[0] is the CPU
      default_device = devices[0];
      std::cout << "Using device: " << default_device.getInfo<CL_DEVICE_NAME>() << "\n";
    } catch (cl::Error &e) {
      result = MLError<int>(BOOST_CURRENT_FUNCTION, __LINE__,
                            std::string(" Get Devices  error: ") + std::string(e.what()) + " "
                                + std::string(reinterpret_cast<const char *>(e.err())));

    }
    return result;
  }

};  // class OpenCLFactory


}  // namespace ML
#endif //MLTEMPLATE_LIBRARY_INCLUDE_OPENCL_TEST_OPENCL_H_
