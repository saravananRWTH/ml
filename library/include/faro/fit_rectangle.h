// COPYRIGHT 2020 DLR
#ifndef MLTEMPLATE_LIBRARY_INCLUDE_FARO_FIT_RECTANGLE_H_
#define MLTEMPLATE_LIBRARY_INCLUDE_FARO_FIT_RECTANGLE_H_
#include <iostream>
#include <memory>
#include<iostream>
#include<fstream>
#include<sstream>
#include <cmath>
#include <algorithm>
using namespace std;
namespace faro {

struct Rectangle {
  int length;
  int breadth;
  int image_id;
  bool flipped = false;
  int max_side;
  Rectangle() = delete;

  int area;

  Rectangle(const int length, const int breadth, const int img_id)
      : length(length), breadth(breadth), image_id(img_id) {
    area = length * breadth;
    length > breadth ? max_side = length : max_side = breadth;
  };
  Rectangle(const Rectangle &rhs) {
    length = rhs.length;
    breadth = rhs.breadth;
    image_id = rhs.image_id;
    area = rhs.area;
    max_side = rhs.max_side;
  }

  void swap() {
    int temp = length;
    length = breadth;
    breadth = temp;
    flipped = !flipped;
  }
};  // struct Rectangle



struct rectangle_coord {
  int left, right, top, bottom, width, height;
  rectangle_coord(int l, int r, int t, int b, int len, int br)
      : left(l), right(r), top(t), bottom(b), width(len), height(br) {
  }

  rectangle_coord() : left(0), right(0), top(0), bottom(0) {};

  rectangle_coord(const rectangle_coord &rhs) {
    left = rhs.left;
    top = rhs.top;
    bottom = rhs.bottom;
    right = rhs.right;
    width = rhs.width;
    height = rhs.height;
  }

  rectangle_coord &operator=(const rectangle_coord &rhs) {
    if (this != &rhs) {
      left = rhs.left;
      top = rhs.top;
      bottom = rhs.bottom;
      right = rhs.right;
      width = rhs.width;
      height = rhs.height;
    }
    return *this;
  }

  void print() {
    cout << "left: " << left << " right: " << right << " top: " << top << " bottom: " << bottom << "\n";
    cout << "length: " << width << " breadth: " << height << "\n";
  }

};

struct pack_tree {
  std::vector<std::shared_ptr<pack_tree>> child{2};
  rectangle_coord rectangle;
  int image_id;
  bool is_partitioned = false;
  bool is_fit = false;

  int get_height() {

  }

  pack_tree() {
    for (auto i:child) {
      i = NULL;
    }
    rectangle = rectangle_coord(0, 0, 0, 0, 0, 0);
    image_id = 0;
  };

  pack_tree(rectangle_coord &rect1) {
    this->rectangle = rect1;
  }

  pack_tree(int left, int right, int top, int bottom,
            int length,
            int breadth
  ) {
    for (auto i:child) {
      i = NULL;
    }
    rectangle = rectangle_coord(left, right, top, bottom, length, breadth);
    image_id = 0;
  };

  pack_tree(std::vector<std::shared_ptr<pack_tree>> child1, rectangle_coord rect, int img_id) {
    for (auto i:child1) {
      child.push_back(i);
    }
    rectangle = rect;
    image_id = img_id;
  }

  std::shared_ptr<pack_tree> insert(Rectangle &rect1) {

    /*
    if (is_fit) {
      return NULL;
    } else if (!is_partitioned) {
      rectangle.print();
      if (is_perfect_fit(rect1)) {
        is_fit = true;
        image_id = rect1.image_id;
        return NULL;
      } else if (is_rect_fit(rect1)) {
        cout << "partioning and fitting the rectangle \n";

        int db = rectangle.breadth - rect1.breadth;
        int dl = rectangle.length - rect1.length;

        rectangle.right = rectangle.length - dl;
        rectangle.bottom = rectangle.breadth - db;
        rectangle.breadth = rect1.breadth;
        rectangle.length = rect1.length;
        image_id = rect1.image_id;
        rectangle.print();
        cout << " db " << db << " dl " << dl << "\n";
        this->child[0] = std::make_shared<pack_tree>();
        this->child[1] = std::make_shared<pack_tree>();

      }

      return NULL;
    } else */

    if (this->child[0] != NULL && this->child[1] != NULL) {
      if (this->child[0] != NULL) {
        std::shared_ptr<pack_tree> new_node = child[0]->insert(rect1);
        if (new_node != NULL) {
          return new_node;
        }
      } else if (this->child[1] != NULL) {
        std::shared_ptr<pack_tree> new_node = child[1]->insert(rect1);
        if (new_node != NULL) {
          return new_node;
        }
      }
    } else {
      if (!is_rect_fit(rect1)) { return NULL; }
      else {
        if (is_perfect_fit(rect1)) {

          this->image_id = rect1.image_id;
          cout << " Perfect image id: " << image_id << "\n";
          is_fit = true;
          return NULL;
        } else {
          int dl = (rectangle.bottom - rectangle.top) - rect1.length;
          int db = (rectangle.right - rectangle.left) - rect1.breadth;
          cout << "dl " << dl << " db " << db << "\n";
          if (image_id == 0) {
            if (db > dl) {
              if (dl == 0) {
                this->child[0] = std::make_shared<pack_tree>();
                this->child[0]->rectangle =
                    rectangle_coord(rectangle.left,
                                    rectangle.right,
                                    rectangle.top + db,
                                    rectangle.bottom,
                                    rect1.length,
                                    rect1.breadth);
              } else {
                this->child[0] = std::make_shared<pack_tree>();
                this->child[1] = std::make_shared<pack_tree>();

                this->child[0]->rectangle =
                    rectangle_coord(rectangle.left,
                                    rectangle.left + rect1.breadth,
                                    rectangle.top,
                                    rectangle.bottom,
                                    rect1.length,
                                    rect1.breadth);

                this->child[1]->rectangle =
                    rectangle_coord(rectangle.left + rect1.breadth,
                                    rectangle.right,
                                    rectangle.top,
                                    rectangle.bottom,
                                    rect1.length,
                                    rect1.breadth);
              }
            } else {
              this->child[0] = std::make_shared<pack_tree>();
              this->child[1] = std::make_shared<pack_tree>();

              this->child[0]->rectangle =
                  rectangle_coord(rectangle.left,
                                  rectangle.right,
                                  rectangle.top,
                                  rectangle.top + rect1.length,
                                  rect1.length,
                                  rect1.breadth);

              this->child[1]->rectangle =
                  rectangle_coord(rectangle.left,
                                  rectangle.right,
                                  rectangle.top + rect1.length,
                                  rectangle.bottom,
                                  rect1.length,
                                  rect1.breadth);
            }

            rectangle.right = rectangle.width - dl;
            rectangle.bottom = rectangle.height - db;
            rectangle.height = rect1.breadth;
            rectangle.width = rect1.length;
            image_id = rect1.image_id;
            is_fit = true;
            rectangle.print();
            cout << "\n";
            return this->child[0];
          }
        }
      }
    }
  }

  bool is_perfect_fit(Rectangle &rect1) {
    bool is_perfect = false;
    int current_rectangle_length = (rectangle.bottom - rectangle.top);
    int current_rectangle_breadth = (rectangle.right - rectangle.left);
//    cout << "Perfect fit: current_rectangle_length " << current_rectangle_length << " current_rectanlge_breadth "<< current_rectangle_breadth << "\n";
    if (current_rectangle_length == rect1.length && current_rectangle_breadth == rect1.breadth) {
      is_perfect = true;
    } else if (current_rectangle_length == rect1.breadth && current_rectangle_breadth == rect1.length) {
      is_perfect = true;
      rect1.swap();
    }

    if (is_perfect) {
      cout << "It is a perfect fit" << "\n";
    }
    return is_perfect;
  }

  bool is_rect_fit(Rectangle &rect1) {
    bool is_fit = false;
    int current_rectangle_length = (rectangle.bottom - rectangle.top);
    int current_rectangle_breadth = (rectangle.right - rectangle.left);
//    cout << "Rect fit: current_rectangle_length " << current_rectangle_length << " current_rectanlge_breadth "     << current_rectangle_breadth << "\n";

    if (current_rectangle_length >= rect1.length && current_rectangle_breadth >= rect1.breadth) {
      is_fit = true;
    } else if (current_rectangle_length >= rect1.breadth && current_rectangle_breadth >= rect1.length) {
      is_fit = true;
      rect1.swap();
    }
    if (is_fit) {
      cout << "rectangle can be fit inside " << "\n";
    }
    return is_fit;
  }

  std::shared_ptr<pack_tree> insert(const rectangle_coord &rect1) {

  }

};  // struct pack_tree

struct area {
  int x, y, size1, size2;
  int height, width;
};

class Pack {
 public:
  enum SORT_ORDER {
    SORT_BY_LENGTH = 0,
    SORT_BY_BREADTH = 1,
    SORT_BY_MAX_SIDE = 2,
    SORT_BY_AREA = 3
  };

  Pack(SORT_ORDER p_sort_order = SORT_BY_MAX_SIDE) {
    m_sort_order = p_sort_order;
  }

  void print_dimension() {
    for (auto i:rect) {
      cout << "length: " << i.length << " breadth: " << i.breadth << "\n";
    }
  }

  void sort_rectangle() {
    auto sort_by_max_side = [](const Rectangle &i, const Rectangle &j) { return i.max_side > j.max_side; };
    auto sort_by_length = [](const Rectangle &i, const Rectangle &j) { return i.length > j.length; };
    auto sort_by_breadth = [](const Rectangle &i, const Rectangle &j) { return i.breadth > j.breadth; };
    auto sort_by_area = [](const Rectangle &i, const Rectangle &j) { return i.area > j.area; };

    switch (m_sort_order) {
      case SORT_BY_LENGTH:std::sort(rect.begin(), rect.end(), sort_by_length);
        break;
      case SORT_BY_BREADTH:std::sort(rect.begin(), rect.end(), sort_by_breadth);
        break;
      case SORT_BY_MAX_SIDE:std::sort(rect.begin(), rect.end(), sort_by_max_side);
        break;
      case SORT_BY_AREA:std::sort(rect.begin(), rect.end(), sort_by_area);
        break;
      default:break;
    }
  }

  void load(std::string file_name) {
    ifstream f(file_name); //taking file as inputstream
    string str;
    string line;
    string delimit = " ";
    int image_id = 1;
    while (getline(f, line)) {
      int length = std::stoi(line.substr(0, line.find(delimit)));
      int breadth = std::stoi(line.substr(line.find(delimit)));
      auto temp = Rectangle(length, breadth, image_id);
      image_id += 1;
      rect.push_back(temp);
    }
    calculate_complete_area();
    sort_rectangle();
    print_dimension();
    root = std::make_shared<pack_tree>(0, min_square_side, 0, min_square_side, min_square_side, min_square_side);

    for (int i = 0; i < 3; i++) {
      root->insert(rect.at(i));
    }
  }

  int find_max_side() {
    int max_side = 0;
    for (auto i:rect) {
      if (max_side < i.max_side) {
        max_side = i.max_side;
      }
    }
    return max_side;
  }

  void calculate_complete_area() {
    int rectange_area = 0;
    for (auto i:rect) {
      rectange_area += i.area;
    }
    int side = floor(sqrt(rectange_area));
    if (pow(side, 2) < rectange_area) {
      side += 1;
    }
    min_square_area = pow(side, 2);

    min_square_side = side > find_max_side() ? side : find_max_side();
    cout << "Minimum square side: ";
    cout << min_square_side << "\n";
  }

  std::vector<Rectangle> rect;
  std::shared_ptr<pack_tree> root = NULL;
  int min_square_area = 0;
  int min_square_side = 0;
  SORT_ORDER m_sort_order = SORT_BY_MAX_SIDE;
};  // class Pack
}  // namespace faro

#endif MLTEMPLATE_LIBRARY_INCLUDE_FARO_FIT_RECTANGLE_H_
