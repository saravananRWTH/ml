#ifndef MLTEMPLATE_LIBRARY_INCLUDE_FARO_FIT_H_
#define MLTEMPLATE_LIBRARY_INCLUDE_FARO_FIT_H_
#include <functional>
#include <memory>
#include <cmath>
#include <iostream>
#include <algorithm>
#include <vector>
#include<fstream>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

using namespace std;
using namespace cv;
namespace faro {

struct rect_img {
  int height;
  int width;
  int image_id;
  int max_side;
  int area;

  rect_img() = delete;

  rect_img(const int width, const int height, const int img_id)
      : height(height), width(width), image_id(img_id) {
    area = height * width;
    height > width ? max_side = height : max_side = width;
  };

  rect_img(const rect_img &rhs) {
    height = rhs.height;
    width = rhs.width;
    image_id = rhs.image_id;
    area = rhs.area;
    max_side = rhs.max_side;
  }

  void swap() {
    int temp = width;
    width = height;
    height = temp;
    flipped = !flipped;
  }

  bool flipped = false;
  int layout_x = 0;
  int layout_y = 0;
  int layout_w = 0;
  int layout_h = 0;
};

struct Area {
  int x, y, h, w;
  Area() : x(0), y(0), w(0), h(0) {};
  Area(int x1, int y1, int w1, int h1) : x(x1), y(y1), h(h1), w(w1) {};
  Area &operator=(const Area &&rhs) {
    if (this != &rhs) {
      x = rhs.x;
      y = rhs.y;
      h = rhs.h;
      w = rhs.w;
    }
    return *this;
  }
};

struct PackNode {
 public:
  PackNode() {}
  explicit PackNode(int width, int height) {
    area = Area(0, 0, width, height);
  }

  explicit PackNode(int x1, int y1, int width, int height) {
    area = Area(x1, y1, width, height);
  }

  PackNode &operator=(PackNode &&rhs) {
    if (this != &rhs) {
      area = std::move(rhs.area);
    }
    return *this;
  }

  void resize_bin(int new_size) {
    area.h = area.h + new_size;
    area.w = area.w + new_size;
    child0 = NULL;
    child1 = NULL;
  }

  function<int()> height = [=]() { return area.h - area.y; };
  function<int()> width = [=]() { return area.w - area.x; };

  void print() {
    cout << "x: " << area.x << " y: " << area.y << " h " << area.h << " w " << area.w << "\n";
  }

  std::shared_ptr<PackNode> insert(int w, int h) {

    if (child0 != NULL) {
      std::shared_ptr<PackNode> node = child0->insert(w, h);
      if (node == NULL) {
        return child1->insert(w, h);
      } else
        return node;
    } else {
      PackNode node(w, h);
      if (node.width() <= width() && node.height() <= height()) {
        child0 = std::make_shared<PackNode>(area.x + node.width(), area.y, area.w, area.y + node.height());
        child1 = std::make_shared<PackNode>(area.x, area.y + node.height(), area.w, area.h);
        return std::make_shared<PackNode>(area.x, area.y, area.x + node.width(), area.y + node.height());
      } else return NULL;
    }
  }

  function<int()> get_x = [=]() {
    return area.x;
  };

  function<int()> get_y = [=]() {
    return area.y;
  };

  function<int()> get_h = [=]() {
    return area.h;
  };

  function<int()> get_w = [=]() {
    return area.w;
  };

  function<void(int)> set_width = [=](int x) {
    area.w = x;
  };

  function<void(int)> set_height = [=](int x) {
    area.h = x;
  };

  std::shared_ptr<PackNode> child0;
  std::shared_ptr<PackNode> child1;
  Area area;
};

class RectLoader {
 public:
  enum SORT_ORDER {
    SORT_BY_HEIGHT = 0,
    SORT_BY_WIDTH = 1,
    SORT_BY_MAX_SIDE = 2,
    SORT_BY_AREA = 3
  };

  RectLoader(const std::string &fn) {
    file_name = fn;
  }

  void set_sort_order(SORT_ORDER s) {
    m_sort_order = s;
  }

  std::vector<rect_img> load() {
    ifstream f(file_name); //taking file as inputstream
    string str;
    string line;
    std::string delimiter = " ";
    int image_id = 1;
    while (getline(f, line)) {
      vector<string> params;
      tokenize(line, delimiter, params);
      image_id = std::stoi(params[0]);
      int height = std::stoi(params[1]);
      int width = std::stoi(params[2]);
      auto temp = rect_img(width, height, image_id);
      rect.push_back(temp);
    }
    return rect;
  }

  void compute_layout() {
    calculate_init_min_side();
    sort_rectangle();
    print_dimension();
    root = std::make_shared<PackNode>(min_square_side, min_square_side);
    pack_rectangle(root);
    cout << "\n";
    cout << " rectangle minimum size " << root->height() << " " << root->width() << "\n";
    cv::Mat img1(min_square_side + 1, min_square_side + 1, CV_8UC3, Scalar(0, 0, 0));

    cv::Point p1 = cv::Point(0, 0);
    cv::Point p2 = cv::Point(min_square_side, min_square_side);
    cv::rectangle(img1, p1, p2, Scalar(255, 255, 255));

    int r = 0;
    for (auto &i:rect) {
      cout << "Height: " << i.height << " Width: " << i.width << "\n ";
      cout << " x: " << i.layout_x << " y: " << i.layout_y << " h: " << i.layout_h << " w: " << i.layout_w << "\n";
      cv::Point p1 = cv::Point(i.layout_x, i.layout_y);
      cv::Point p2 = cv::Point(i.layout_w, i.layout_h);
      cv::rectangle(img1, p1, p2, Scalar(255, 255, 255));
    }

    string fn = "";
    switch (m_sort_order) {
      case SORT_BY_HEIGHT:fn = "Height Sort";
        break;
      case SORT_BY_WIDTH:fn = "Width Sort";
        break;
      case SORT_BY_MAX_SIDE:fn = "MaxSide Sort";
        break;
      case SORT_BY_AREA:fn = "Area Sort";
        break;
      default:break;
    }

    if (optimize_flag) {
      fn += " Resized area= ";
    } else
      fn += " area= " + std::to_string(min_square_side);
    cv::namedWindow(fn);
    cv::imshow(fn, img1);
    fn += ".bmp";
    string path = "/home/palanisamy/Desktop/Dlr/PR/MLTemplate/library/test/faro/" + fn;
    cout << path << "\n";
    cv::imwrite(path, img1);
  }

 private:

  void pack_rectangle(std::shared_ptr<PackNode> node) {
    for (int i = 0; i < rect.size(); i++) {
      auto uv = node->insert(rect[i].width, rect[i].height);
      if (uv.get() == NULL) {
        rect[i].swap();
        auto uv = node->insert(rect[i].width, rect[i].height);
        if (uv.get() == NULL) {
//          cout << "packaging is wrong again " << "\n";
          root->resize_bin(1);
          min_square_side = root->area.h;
//          cout << "the changed minimum square side " << min_square_side << "\n";
          has_resized = true;

          break;
        } else {
          rect[i].layout_x = uv->get_x();
          rect[i].layout_y = uv->get_y();
          rect[i].layout_w = uv->get_w();
          rect[i].layout_h = uv->get_h();
        }
      } else {
        rect[i].layout_x = uv->get_x();
        rect[i].layout_y = uv->get_y();
        rect[i].layout_w = uv->get_w();
        rect[i].layout_h = uv->get_h();
      }
    }
    if (has_resized) {
      has_resized = false;
      pack_rectangle(root);
    }

  }

  void print_dimension() {
    for (auto i:rect) {
      cout << "height: " << i.height << " width: " << i.width << "\n";
    }
  }

  void sort_rectangle() {
    auto sort_by_max_side = [](const rect_img &i, const rect_img &j) { return i.max_side > j.max_side; };
    auto sort_by_height = [](const rect_img &i, const rect_img &j) { return i.height > j.height; };
    auto sort_by_width = [](const rect_img &i, const rect_img &j) { return i.width > j.width; };
    auto sort_by_area = [](const rect_img &i, const rect_img &j) { return i.area > j.area; };

    switch (m_sort_order) {
      case SORT_BY_HEIGHT:std::sort(rect.begin(), rect.end(), sort_by_height);
        break;
      case SORT_BY_WIDTH:std::sort(rect.begin(), rect.end(), sort_by_width);
        break;
      case SORT_BY_MAX_SIDE:std::sort(rect.begin(), rect.end(), sort_by_max_side);
        break;
      case SORT_BY_AREA:std::sort(rect.begin(), rect.end(), sort_by_area);
        break;
      default:break;
    }
  }

  int find_max_side() {
    int max_side = 0;
    for (auto i:rect) {
      if (max_side < i.max_side) {
        max_side = i.max_side;
      }
    }
    return max_side;
  }

  void calculate_init_min_side() {
    int rectange_area = 0;
    for (auto i:rect) {
      rectange_area += i.area;
    }
    int side = floor(sqrt(rectange_area));
    if (pow(side, 2) < rectange_area) {
      side += 1;
    }
    min_square_area = pow(side, 2);
    min_square_side = side > find_max_side() ? side : find_max_side();
    cout << " Initial Minimum square side: ";
    cout << min_square_side << "\n";
  }

  void tokenize(string &str, std::string delim, vector<string> &out) {
    size_t start;
    size_t end = 0;
    while ((start = str.find_first_not_of(delim, end)) != string::npos) {
      end = str.find(delim, start);
      out.push_back(str.substr(start, end - start));
    }
  }

  std::vector<rect_img> rect;
  std::shared_ptr<PackNode> root = NULL;
  int min_square_area = 0;
  int min_square_side = 0;
  SORT_ORDER m_sort_order = SORT_BY_MAX_SIDE;
  std::string file_name = "";
  bool has_resized = false;
  bool optimize_flag = false;
};  // class Pack

};

#endif
