__kernel void LpNorm(const int d,const float n,__global float* C, 
          __global float* A){

   int tx = get_global_id(0); 
	
   // value stores the element that is 
   // computed by the thread
   float value = 0;
   for (int k=0;k<d;k++){
      // row*N + i
   value+=pow(A[tx*d+k],n);
   }		 
    value=pow(value,1/n);
   // Write the matrix to device memory each 
   // thread writes one element
   C[tx] = value;
}


__kernel void myGEMM2(const int M, const int N, const int K,
                      const __global float* A,
                      const __global float* B,
                      __global float* C){

                      }
