//
// Created by palanisamy on 11.09.20.
//

// COPYRIGHT 2020 Saravanan Palanisamy
#ifndef MLTEMPLATE_STATS_LPNORM_H_
#define MLTEMPLATE_STATS_LPNORM_H_
#pragma once
#include <cstdio> // used to access C style IO (e.g. printf)
#include <iostream> // used to access C++ style IO
#include <vector> // used to access C++ vector types
#include <algorithm>
#include <iterator>

#include "opencl/Opencl.h"
#include "util/utils.h"

using namespace std;
using namespace cl;
namespace ML {
class LpNorm : public OpenCLFactory {

 public:
  LpNorm() : OpenCLFactory() {
    this->setHostProgram();
  }

  ~LpNorm() {
  }

  MLResult<std::vector<float>> calculate_lp_norm(std::vector<std::vector<float>> &points,
                                                 const float norm) {
    const int dimension = points[0].size();
    float lpNorm[points.size()];
    MLResult<std::vector<float>> result;
    try {
      float points_array[points.size() * points[0].size()];
      auto *ptrArray = points_array;
      for (int i = 0; i <= points.size() - 1; i++) {
        lpNorm[i] = 0.0;
        std::copy(points[i].begin(), points[i].end(), ptrArray);
        ptrArray += points[i].size();
      }

      auto kernel_points =
          cl::Buffer(context,
                     CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                     points.size() * points[0].size() * sizeof(float), (void *) &points_array[0]);

      auto kernel_lpNorm =
          cl::Buffer(context,
                     CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR,
                     points.size() * sizeof(float),
                     (void *) &lpNorm[0]);

      this->createProgramKernel("LpNorm.cl", "LpNorm");

      err = kernel.setArg(0, dimension);
      err_check(err, "clSetKernelArg0");
      err = kernel.setArg(1, norm);
      err_check(err, "clSetKernelArg1");
      err = kernel.setArg(2, kernel_lpNorm);
      err_check(err, "clSetKernelArg2");
      err = kernel.setArg(3, kernel_points);
      err_check(err, "clSetKernelArg3");


      // Do the work
      err = queue.enqueueNDRangeKernel(
          kernel,
          cl::NullRange,
          cl::NDRange(points.size()),
          cl::NullRange);

      // Map cBuffer to host pointer. This enforces a sync with
// the host backing space; remember we chose a GPU device.
      int *output = (int *) queue.enqueueMapBuffer(
          kernel_lpNorm,
          CL_TRUE, // block
          CL_MAP_READ,
          0,
          points.size() * sizeof(float));
// Finally release our hold on accessing the memory
      err = queue.enqueueUnmapMemObject(
          kernel_lpNorm,
          (void *) output);

      std::vector<float> f;
      copy(&lpNorm[0], &lpNorm[points.size()], back_inserter(f));
      result = f;
    } catch (cl::Error &e) {
      result = MLError<std::vector<float>>(BOOST_CURRENT_FUNCTION, __LINE__,
                                           std::string(" Get Context  error: ") + std::string(e.what()) + " "
                                               + std::string(reinterpret_cast<const char *>(e.err())));

    }
    return result;
  }
};  // class LpNorm
}// namespace ML
#endif // MLTEMPLATE_STATS_LPNORM_H_

