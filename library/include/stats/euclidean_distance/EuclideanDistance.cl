__kernel void EuclideanDistance(__global float* C, 
          __global float* A, __global float* B){

   int tx = get_global_id(0); 
   // value stores the element that is 
   // computed by the thread
   float value = 0;
   float value1=0;
   float X2minusX1=0;
   float Y2minusY1=0;
   const float norm=2.0;     

    X2minusX1=B[tx*2]-A[tx*2];
    Y2minusY1=B[tx*2+1]-A[tx*2+1];

    value=pow(X2minusX1,2)+pow(Y2minusY1,2);
    printf("%f %f %f\n",X2minusX1,Y2minusY1,value);
    value1=pow(value,1/norm);
      printf("%f  \n \n",value1);

   // Write the matrix to device memory each 
   // thread writes one element
   C[tx] = value1;
}


