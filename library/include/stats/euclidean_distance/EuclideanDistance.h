//
// Created by palanisamy on 23.09.20.
//

// COPYRIGHT 2020 Saravanan Palanisamy
#ifndef MLTEMPLATE_LIBRARY_INCLUDE_STATS_EUCLIDEAN_DISTANCE_EUCLIDEANDISTANCE_H_
#define MLTEMPLATE_LIBRARY_INCLUDE_STATS_EUCLIDEAN_DISTANCE_EUCLIDEANDISTANCE_H_
#pragma once
#include <cstdio> // used to access C style IO (e.g. printf)
#include <iostream> // used to access C++ style IO
#include <vector> // used to access C++ vector types
#include <algorithm>
#include <iterator>
#include <type_traits>

#include "../../opencl/Opencl.h"
#include "../../util/utils.h"

using namespace std;
using namespace cl;
namespace ML {
class EuclideanDistance : public OpenCLFactory {

 public:
  EuclideanDistance() {
    this->setHostProgram();
  }

  ~EuclideanDistance() {
  }

  MLResult<std::vector<float>> calculate_euclidean_distance(std::vector<std::vector<float>> &points1,
                                                            std::vector<std::vector<float>> &points2) {
    float euclid_dist[points1.size()];
    MLResult<std::vector<float>> result;
    try {
      if (points1.size() != points2.size()) {
        throw ("The number of points are not equal ");
      }

      float points1_array[points1.size() * points1[0].size()];
      auto *ptrArray = points1_array;
      for (int i = 0; i < points1.size(); i++) {
        std::copy(points1[i].begin(), points1[i].end(), ptrArray);
        ptrArray += points1[i].size();
      }

      float points2_array[points2.size() * points2[0].size()];
      auto *ptrArray2 = points2_array;
      for (int i = 0; i < points2.size(); i++) {
        std::copy(points2[i].begin(), points2[i].end(), ptrArray2);
        ptrArray2 += points2[i].size();
      }

      auto kernel_points1 =
          cl::Buffer(context,
                     CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                     points1.size() * points1[0].size() * sizeof(float), &points1_array[0]);

      auto kernel_points2 =
          cl::Buffer(context,
                     CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                     points2.size() * points2[0].size() * sizeof(float), &points2_array[0]);

      auto kernel_euclid_dist =
          cl::Buffer(context,
                     CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR,
                     points1.size() * sizeof(float),
                     (void *) &euclid_dist[0]);

      this->createProgramKernel("EuclideanDistance.cl", "EuclideanDistance");
      err = kernel.setArg(1, kernel_points1);
      err = kernel.setArg(2, kernel_points2);
      err = kernel.setArg(0, kernel_euclid_dist);
      // Do the work
      err = queue.enqueueNDRangeKernel(
          kernel,
          cl::NullRange,
          cl::NDRange(points1.size()),
          cl::NullRange);

      // Map cBuffer to host pointer. This enforces a sync with
// the host backing space; remember we chose a GPU device.
      int *output = (int *) queue.enqueueMapBuffer(
          kernel_euclid_dist,
          CL_TRUE, // block
          CL_MAP_READ,
          0,
          points1.size() * sizeof(float));

      std::vector<float> f;
      copy(&euclid_dist[0], &euclid_dist[points1.size()], back_inserter(f));

      result = f;

      // Finally release our hold on accessing the memory
      err = queue.enqueueUnmapMemObject(
          kernel_euclid_dist,
          (void *) output);

    } catch (cl::Error &e) {
      result = MLError<std::vector<float>>(BOOST_CURRENT_FUNCTION, __LINE__,
                                           std::string(" Get Context  error: ") + std::string(e.what()) + " "
                                               + std::string(reinterpret_cast<const char *>(e.err())));
    }
    return result;
  }

};  // class EUclideanDistance
}  // namespace ML

#endif //MLTEMPLATE_LIBRARY_INCLUDE_STATS_EUCLIDEAN_DISTANCE_EUCLIDEANDISTANCE_H_
