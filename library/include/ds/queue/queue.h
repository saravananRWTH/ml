// COPYRIGHT 2020 DLR
#ifndef MLTEMPLATE_LIBRARY_INCLUDE_DS_QUEUE_QUEUE_H_
#define MLTEMPLATE_LIBRARY_INCLUDE_DS_QUEUE_QUEUE_H_
#include <iostream>
#include <memory>
using namespace std;
namespace ds {

template<class U>
struct QueueNode {
  U data;
  std::shared_ptr<QueueNode<U>> next;
  std::shared_ptr<QueueNode<U>> previous;
  QueueNode(int data) : data{data}, next{nullptr}, previous{nullptr} {}
};

enum TYPE_OF_QUEUE {
  SIMPLE_QUEUE = 0,
  DOUBLE_ENDED_QUEUE = 1,
  CIRCULAR_QUEUE = 2,
  PRIORITY_QUEUE = 3
};

template<class U, enum TYPE_OF_QUEUE queue_type = SIMPLE_QUEUE>
class Queue {
 public:

  Queue() {}
  explicit Queue(U data, int size) : data(data), head(nullptr), tail(nullptr), default_size(size) {
    size_enable = true;
  }
  explicit Queue(U data) : data(data) {
    default_size = 1;
    size_enable = false;
  }

  bool is_full() {
    if (size_enable && default_size == size) {
      return true;
    } else
      return false;
  }

  void front_enqueue(U data) {
    auto temp{std::make_shared<QueueNode<U>>(data)};
    if (size == 0) {
      tail = temp;
      head = temp;
      size++;
      default_size = size;
    } else {
      if (is_full()) {
        cout << " Queue Overflow error " << "\n";
      } else {
        temp->next = head;
        head->previous = temp;
        head = std::move(temp);
        size++;
      }
    }
  }

  void back_enqueue(U data) {
    auto temp{std::make_shared<QueueNode<U>>(data)};
    if (size == 0) {
      tail = temp;
      head = temp;
      size++;
      default_size = size;
    } else {
      if (is_full()) {
        cout << " Queue Overflow error " << "\n";
      } else {
        temp->previous = tail;
        tail->next = temp;
        tail = std::move(temp);
        size++;
      }
    }
  }

  void front_dequeue() {
    if (is_empty()) {
      cout << "there is no element in queue \n";
    } else {
      head = std::move(head->next);
      size--;
    }
  }

  void back_dequeue() {
    if (is_empty()) {
      cout << "there is no element in queue \n";
    } else {
      tail = std::move(tail->previous);
      tail->next = nullptr;
      size--;
    }
  }

  bool is_empty() {
    if (default_size == 0)
      return true;
    else
      return false;
  }

  void print() {
    auto temp = head.get();
    cout << " queue: ";
    while (temp) {
      cout << temp->data << " ";
      temp = temp->next.get();
    }
    cout << "\n";
  }

 private:
  std::shared_ptr<QueueNode<U>> head;
  std::shared_ptr<QueueNode<U>> tail;
  int size = 0;
  int default_size = 0;
  U data;
  bool size_enable = false;
};

}  // namespace ds
#endif MLTEMPLATE_LIBRARY_INCLUDE_DS_QUEUE_QUEUE_H_
