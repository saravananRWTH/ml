// COPYRIGHT 2020 DLR
#ifndef MLTEMPLATE_LIBRARY_INCLUDE_DS_HEAP_HEAP_H_
#define MLTEMPLATE_LIBRARY_INCLUDE_DS_HEAP_HEAP_H_
#include "../../ds/bt/bt.h"
#include <memory>
using namespace std;
namespace ds {
using ds::BT_Node;

template<class U>
class Heap : public Binary_Tree<U> {
 public:
  enum HEAP_TYPE {
    MAX_HEAP = 0,
    MIN_HEAP = 1
  };

  Heap(HEAP_TYPE h = MAX_HEAP) : m_heap_type(h) {
    switch (m_heap_type) {
      case MAX_HEAP:cout << "Heap is max -heap " << "\n ";
        break;
      case MIN_HEAP:cout << "Heap is min heap " << "\n";
        break;
      default:break;
    }
  }

  void set_heap_type() {
    m_heap_type = MAX_HEAP;
  }

  void create_complete_bt(std::vector<U> data) {
    this->create_complete(data);
  }

  void swap(std::shared_ptr<BT_Node<U>> key, std::shared_ptr<BT_Node<U>> current) {
    U temp = key->data;
    key->data = current->data;
    current->data = temp;
  }

  void max_heapify(std::shared_ptr<BT_Node<U>> node, int total_nodes, int index) {
    if (node->left == NULL) {
      return;
    }
    if (node->left != NULL && node->data < node->left->data) {
      swap(node, node->left);
    }
    if (node->right != NULL && node->data < node->right->data) {
      swap(node, node->right);
    }
    max_heapify(node->left, total_nodes, 2 * index + 1);
    if (node->right != NULL)
      max_heapify(node->right, total_nodes, 2 * index + 2);
  }

  void insert_at_end(std::shared_ptr<BT_Node<U>> node, U data, int total_nodes, int index = 0) {
    if (index * 2 + 1 == total_nodes) {
      node->left = std::make_shared<BT_Node<U>>(data);
    } else if (index * 2 + 2 == total_nodes) {
      node->right = std::make_shared<BT_Node<U>>(data);
    }
    if (node->left != NULL) {
      insert_at_end(node->left, data, total_nodes, 2 * index + 1);
    }
    if (node->right != NULL) {
      insert_at_end(node->right, data, total_nodes, 2 * index + 2);
    }
    heapify();
  }

  void find_node(std::shared_ptr<BT_Node<U>> node, U data, U replace_data) {
    if (node == NULL)
      return;
    else if (node != NULL) {
      if (node->data == data) {
//        cout << " the data " << data << " " << replace_data << " \n";
        node->data = replace_data;
      } else {
        if (node->left != NULL) {
          find_node(node->left, data, replace_data);
        }
        if (node->right != NULL) {
          find_node(node->right, data, replace_data);
        }
      }
    }
  }

  auto find_node(U data) {

  }

  U maximum_value() {
    auto root = this->get_root();
    if (root == NULL)
      return -1;
    else
      return root->data;
  }

  U extract_maximum() {
    auto root = this->get_root();
    U max;
    if (root == NULL)
      return -1;
    else {
      max = root->data;
      delete_node(max);
    }

    return max;
  }

  void delete_node(std::shared_ptr<BT_Node<U>> node, U data, int total_nodes, int index = 0) {
//    cout << "total nodes " << total_nodes << " \n";
    if (total_nodes == 1 && index == 0) {
      node = NULL;
    } else if (2 * index + 1 == total_nodes - 1) {
      U temp = node->left->data;
      cout << temp << " \n";
      node->left->data = data;
      find_node(this->get_root(), data, temp);
      node->left = NULL;
    } else if (2 * index + 2 == total_nodes - 1) {
      U temp = node->right->data;
      node->right->data = data;
      find_node(this->get_root(), data, temp);
      node->right = NULL;
    } else {
      if (node->left != NULL)
        delete_node(node->left, data, total_nodes, 2 * index + 1);
      if (node->right != NULL)
        delete_node(node->right, data, total_nodes, 2 * index + 2);
    }

  }

  void delete_node(U data) {
    auto root = this->get_root();
    auto temp = std::make_shared<BT_Node<U>>(data);
    if (root == NULL) {
      return;
    } else
      delete_node(root, data, this->totalNumberOfNodes());

    heapify();
  }

  void insert_node(U data) {
    auto root = this->get_root();
    auto temp = std::make_shared<BT_Node<U>>(data);
    if (root == NULL) {
      this->set_root(temp);
    } else
      insert_at_end(root, data, this->totalNumberOfNodes());
  }

  void inorder(std::shared_ptr<BT_Node<U>> node) {
    if (node == NULL)
      return;
    inorder(node->left);
    cout << node->data << " ";
    inorder(node->right);
  }

  void print_heap(std::shared_ptr<BT_Node<U>> node) {
    inorder(node);
    cout << "\n";
  }

  void heapify() {
    auto root = this->get_root();
    if (root == nullptr) {
      return;
    } else if (root->left == NULL && root->right == NULL) {
      return;
    } else
      switch (m_heap_type) {
        case MAX_HEAP:max_heapify(root, this->totalNumberOfNodes(root), 0);
          break;
        case MIN_HEAP:break;
        default:break;
      }

  }

 private:
  HEAP_TYPE m_heap_type;

};

}

#endif MLTEMPLATE_LIBRARY_INCLUDE_DS_HEAP_HEAP_H_
