// COPYRIGHT 2020 DLR
#ifndef MLTEMPLATE_LIBRARY_INCLUDE_DS_STACK_STACK_H_
#define MLTEMPLATE_LIBRARY_INCLUDE_DS_STACK_STACK_H_
#include <iostream>
#include <memory>
using namespace std;
namespace ds {

template<class U>
struct Node {
  U data;
  std::unique_ptr<Node<U>> next;
  Node(U data) : data{data}, next{nullptr} {}
};

template<class U>
class Stack {
 public:
  Stack() : head(nullptr) {}
  Stack(U data, int size1 = 1) {
    head = std::make_unique<Node<U>>(data);
    size++;
    default_size = size1;
  }

  void create(U data, int size1 = 1) {
    if (head == nullptr) {
      head = std::make_unique<Node<U>>(data);
    } else {
      cout << " head is not empty " << "\n";
    }
    size++;
    default_size = size1;
  }

  void push(U data) {
    auto temp{std::make_unique<Node<U>>(data)};
    if (isFull()) {
      cout << "Error overflow stack" << "\n";
    } else if (head) {
      temp->next = std::move(head);
      head = std::move(temp);
      size++;
    } else {
      head = std::move(temp);
      size++;
    }
  }

  void expand(int size1) {
    if (size1 > default_size)
      default_size = size1;
    else
      cout << "cannot decrease the size " << "\n";
  }

  void pop() {
    if (!isEmpty()) {
      head = std::move(head->next);
      size--;
    } else {
      cout << " Stack is empty " << "\n";
    }
  }

  bool isEmpty() {
    if (head == nullptr && size == 0) {
      return true;
    } else return false;
  }

  U peek() {
    cout << "Peek data " << head->data << "\n ";
    return head->data;
  }

  bool isFull() {
    if (size == default_size)
      return true;
    else
      return false;
  }

  void clean() {
    while (!isEmpty()) {
      head = std::move(head->next);
      --size;
    }
  }

  void print() {
    auto traverse = head.get();
    while (traverse) {
      cout << traverse->data << "\n";
      traverse = traverse->next.get();
    }
    cout << "end " << "\n";
  }

 private:
  std::unique_ptr<Node<U>> head;
  int size = 0;
  int default_size = 0;
};

}

#endif MLTEMPLATE_LIBRARY_INCLUDE_DS_STACK_STACK_H_
