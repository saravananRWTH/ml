// COPYRIGHT 2020 DLR
#ifndef MLTEMPLATE_LIBRARY_INCLUDE_DS_BUBBLE_SORT_BUBBLE_SORT_H_
#define MLTEMPLATE_LIBRARY_INCLUDE_DS_BUBBLE_SORT_BUBBLE_SORT_H_
#include <iostream>
#include <memory>
#include<vector>
#include <math.h>
using namespace std;
namespace algorithm {

enum SORT {
  BUBBLE_SORT = 0,
  SELECTION_SORT = 1,
  INSERTION_SORT = 2,
  MERGE_SORT = 3,
  QUICK_SORT = 4,
  COUNT_SORT = 5,
  RADIX_SORT = 6,
  BUCKET_SORT = 7,
  HEAP_SORT = 8,
  SHELL_SORT = 9
};

template<class T>
struct bucket {
  T data;
  std::shared_ptr<bucket<T>> next;
  //   std::unique_ptr<bucket<U>> next;
  bucket() : next{nullptr} {}
  bucket(T data) : data{data}, next{nullptr} {}
  bucket operator=(const bucket &value) {
    if (this != value) {
      data = value.data;
      next = value.next;
    }
    return *this;
  }
};

template<typename U>
class Sort {
 public:
  Sort(SORT sort_type) : sort_type(sort_type) {};

  void sort(std::vector<U> &list) {
    switch (sort_type) {
      case BUBBLE_SORT:bubble_sort(list);
        break;
      case SELECTION_SORT:selection_sort(list);
        break;
      case INSERTION_SORT: insertion_sort(list);
        break;
      case MERGE_SORT: merge_sort(list);
        break;
      case QUICK_SORT: quick_sort(list);
        break;
      case COUNT_SORT: count_sort(list);
        break;
      case RADIX_SORT: radix_sort(list);
        break;
      case BUCKET_SORT:bucket_sort(list);
        break;
      case HEAP_SORT: heap_sort(list);
        break;
      case SHELL_SORT:shell_sort(list);
        break;
      default:break;
    }
  }

  void set_type(SORT sort_type1) {
    sort_type = (SORT) sort_type1;
  }

  void print(std::vector<U> list) {
    for (auto i:list) {
      cout << i << " ";
    }
    cout << "\n";
  }

 private:
  SORT sort_type;
  void swap(U &i, U &j) {
    U temp = i;
    i = j;
    j = temp;
  }

  void shell_sort(std::vector<U> &list) {
    int size = list.size();
    int n = 1;

    int d = size / (2 * n);
    while (size / (2 * n) >= 1) {
      for (int i = 0; i < size; i++) {
        int r = size / (2 * n);
        if ((i + r) <= size - 1) {
          if (list[i] > list[i + r]) {
            swap(list[i], list[i + r]);
          }
          if (i + r > size - 1) {
            break;
          }
        }
      }
      n++;
    }
  }

  void heapify(std::vector<U> &list, int size, int index) {

    int largest = index; // Initialize largest as root
    int l = 2 * index + 1; // left = 2*i + 1
    int r = 2 * index + 2; // right = 2*i + 2

    // If left child is larger than root
    if (l < size && list[l] > list[largest])
      largest = l;

    // If right child is larger than largest so far
    if (r < size && list[r] > list[largest])
      largest = r;

    // If largest is not root
    if (largest != index) {
      swap(list[index], list[largest]);

      // Recursively heapify the affected sub-tree
      heapify(list, size, largest);
    }
  }

  void heap_sort(std::vector<U> &list) {
    for (int i = list.size() / 2 - 1; i >= 0; i--) {
      heapify(list, list.size() - 1, i);
    }

    for (int i = list.size() - 1; i >= 0; i--) {
      swap(list[0], list[i]);

      // Heapify root element to get highest element at root again
      heapify(list, i, 0);
    }

  }

  void bubble_sort(std::vector<U> &list) {
    bool swapped = false;
    for (int i = 0; i < list.size() - 1; i++) {
      for (int j = 0; j < list.size() - 1; j++) {
        if (list[j] > list[j + 1]) {
          /*         U temp = list[j];
                   list[j] = list[j + 1];
                   list[j + 1] = temp;*/
          swap(list[j], list[j + 1]);
          swapped = true;
        }
      }
      if (!swapped) {
        break;
      }
      swapped = false;
    }
  }  // function bubble_sort

  void selection_sort(std::vector<U> &list) {
    for (int i = 0; i < list.size() - 1; i++) {
      int minimum_index = i;
      int minimum = list[i];
      for (int j = i + 1; j < list.size(); j++) {
        if (list[j] < minimum) {
          minimum = list[j];
          minimum_index = j;
        }
      }
      swap(list[i], list[minimum_index]);
    }
  }  // function selection_sort

  void insertion_sort(std::vector<U> &list) {
    for (int i = 1; i < list.size(); i++) {
      U key = list[i];
      for (int j = i - 1; j >= 0; j--) {
        if (key < list[j]) {
          list[j + 1] = list[j];
          list[j] = key;
        }
      }
    }
  }

  void merge_listay(std::vector<U> &list, int p, int q, int r) {
    int n1 = q - p + 1;
    int n2 = r - q;
    int i = 0, j = 0, k = p;
    std::vector<U> L(n1);
    std::vector<U> R(n2);
    for (int i = 0; i < n1; i++) {
      L[i] = list[p + i];
    }

    for (int j = 0; j < n2; j++) {
      R[j] = list[q + 1 + j];
    }

    while (i < n1 && j < n2) {

      if (L[i] <= R[j]) {
        list[k] = L[i];
        i++;
      } else {
        list[k] = R[j];
        j++;
      }
      k++;
    }

    while (i < n1) {
      list[k] = L[i];
      i++;
      k++;
    }

    while (j < n2) {
      list[k] = R[j];
      j++;
      k++;
    }

  }

  void merge_sort(std::vector<U> &list, int p, int r) {
    if (p < r) {
      int q = (p + r) / 2;
      merge_sort(list, p, q);
      merge_sort(list, q + 1, r);
      merge_listay(list, p, q, r);
    } else
      return;
  }

  void merge_sort(std::vector<U> &list) {
    merge_sort(list, 0, list.size() - 1);
  }

  int partition(std::vector<U> &list, int l, int r) {
    U pivot_element = list[r];
    int store_index = l - 1;
    for (int i = l; i < r; i++) {
      if (list[i] <= pivot_element) {
        store_index++;
        swap(list[store_index], list[i]);
      }
    }
    swap(list[store_index + 1], list[r]);
    return store_index + 1;
  }

  void quick_sort(std::vector<U> &list, int l, int r) {
    U pivot_element = list[r];
    if (l < r) {
      int pivot_index = partition(list, l, r);
      quick_sort(list, l, pivot_index - 1);
      quick_sort(list, pivot_index + 1, r);
    }
  }

  void quick_sort(std::vector<U> &list) {
    quick_sort(list, 0, list.size() - 1);
  }

  U max_element(std::vector<U> &list) {
    U max_element = 0;
    for (auto i:list) {
      if (max_element < i)
        max_element = i;
    }
    return max_element;
  }

  void count_sort(std::vector<U> &list) {
    U max = max_element(list);
    cout << "max_count is " << max << "\n";
    std::vector<int> list1(max + 1);
    std::fill(list1.begin(), list1.end(), 0);
    int cum_sum = 0;
    for (auto i:list) {
      list1[i] += 1;
    }

    cum_sum = list1[0];
    for (int i = 0; i < list1.size(); i++) {
      cum_sum += list1[i];
      list1[i] = cum_sum;
    }
    std::vector<U> list2(list.size());

    for (auto i:list) {
      int index = list1[i] - 1;
      list2[index] = i;
      list1[i] = index;
    }
    list = std::move(list2);
  }

  void count_sort_radix(std::vector<U> &list, int exp) {
    int output[list.size()]; // output listay
    int i, count[10] = {0};

    // Store count of occurrences in count[] 
    for (i = 0; i < list.size(); i++) {
      count[(list[i] / exp) % 10]++;
    }

    // Change count[i] so that count[i] now contains actual 
    //  position of this digit in output[] 
    for (i = 1; i < 10; i++)
      count[i] += count[i - 1];

    // Build the output listay 
    for (i = list.size() - 1; i >= 0; i--) {
      output[count[(list[i] / exp) % 10] - 1] = list[i];
      count[(list[i] / exp) % 10]--;
    }

    // Copy the output listay to list[], so that list[] now 
    // contains sorted numbers according to current digit 
    for (i = 0; i < list.size(); i++)
      list[i] = output[i];
  }

  void radix_sort(std::vector<U> &list) {
    U max = max_element(list);

    for (int exp = 1; max / exp > 0; exp *= 10)
      count_sort_radix(list, exp);
  }

  void bucket_sort(std::vector<U> &list) {
    std::vector<std::vector<U>> buckets[list.size()];
    U max = max_element(list);
    // buckets[floor(list.size() * list[i] / max_element)] = list[i];
    // 1) Create n empty buckets

    // 2) Put array elements in different buckets
    for (int i = 0; i < list.size(); i++) {
      int bi = floor(list.size() * list[i] / max);
      cout << bi << " ";
    }

    cout << "\n";
  }

};

}
#endif MLTEMPLATE_LIBRARY_INCLUDE_DS_BUBBLE_SORT_BUBBLE_SORT_H_
