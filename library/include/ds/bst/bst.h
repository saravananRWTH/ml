// COPYRIGHT 2020 Saravanan Palanisamy
#ifndef MLTEMPLATE_LIBRARY_INCLUDE_DS_BST_BST_H_
#define MLTEMPLATE_LIBRARY_INCLUDE_DS_BST_BST_H_
#pragma once
#include <iostream>
#include <memory>

using namespace std;
namespace ds {

template<class U>
struct bst_node {
  U data;
  std::shared_ptr<bst_node<U>> left = NULL;
  std::shared_ptr<bst_node<U>> right = NULL;
  bst_node(U data) : data(data), left(nullptr), right(nullptr) {};
};

template<class U>
class BST {
 public:

  enum TRAVERSAL_ORDER1 {
    IN_ORDER = 0,
    PRE_ORDER = 1,
    POST_ORDER = 2
  };

  void insert(U value) {
    auto temp = std::make_shared<bst_node<U>>(value);
    if (root == nullptr) {
      root = std::move(temp);
    } else {
      insert(root, value);
    }
  }

  auto insert(std::shared_ptr<bst_node<U>> node, U value) {
    if (node == NULL) {
      return std::make_shared<bst_node<U>>(value);
    } else if (value < node->data) {
      node->left = insert(node->left, value);
    } else if (value >= node->data) {
      node->right = insert(node->right, value);
    }
    return node;
  }

  void inorder(std::shared_ptr<bst_node<U>> node) {
    if (node == NULL) {
      return;
    }
    inorder(node->left);
    cout << node.get()->data << " ";
    inorder(node.get()->right);
  }

  void postorder(std::shared_ptr<bst_node<U>> node) {
    if (node.get() == nullptr) {
      return;
    }
    postorder(node->left);
    postorder(node->right);
    cout << node->data << " ";
  }

  void preorder(std::shared_ptr<bst_node<U>>
                node) {
    if (node.get() == nullptr) {
      return;
    }
    cout << node->data << " ";
    preorder(node->left);
    preorder(node->right);
  }

  bool search(U data) {
    search(root, data);
  }

  bool search(std::shared_ptr<bst_node<U>> node, U data) {
    if (node == NULL) {
      cout << data << " not found \n";
      return false;
    } else if (data == node->data) {
      cout << data << " data found \n";
      return true;
    } else if (data > node->data) {
      search(node->right, data);
    } else if (data < node->data) {
      search(node->left, data);
    } else {
      cout << data << " not found \n";
      return false;
    }
  }

  void remove(U data) {
    remove(root, data);
  }

  void remove(std::shared_ptr<bst_node<U>> &node, U data) {
    if (node == NULL) {
      cout << "cannot remove which is not present " << "\n";
    } else if (data == node->data) {
      cout << " data found " << "\n";
      if (node->left == NULL && node->right == NULL) {
        cout << "data is " << "\n";
        node.reset();
      } else if ((node->left != NULL && node->right == NULL)) {
        cout << "data: ";
        cout << node->data << " ";
        node = std::move(node->left);
        cout << node->data << " \n";
      } else if ((node->right != NULL && node->left == NULL)) {
        cout << "data: ";
        cout << node->data << " ";
        node = std::move(node->right);
        cout << node->data << " \n";
      } else if ((node->right != NULL) && (node->left != NULL)) {
        cout << "inorder successor \n";

      }
    } else if (data < node->data) {
      remove(node->left, data);
    } else if (data > node->data) {
      remove(node->right, data);
    }
  }

  void print(TRAVERSAL_ORDER1 to) {
    cout << "Binary tree: ";
    switch (to) {
      case PRE_ORDER:preorder(root);
        break;
      case POST_ORDER:postorder(root);
        break;
      case IN_ORDER:inorder(root);
        break;
      default:break;
    }
    cout << "\n";
  }

 private:
  std::shared_ptr<bst_node<U>> root;
};

}
#endif MLTEMPLATE_LIBRARY_INCLUDE_DS_BST_BST_H_
