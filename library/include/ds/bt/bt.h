// COPYRIGHT 2020 DLR
#ifndef MLTEMPLATE_LIBRARY_INCLUDE_DS_BT_BT_H_
#define MLTEMPLATE_LIBRARY_INCLUDE_DS_BT_BT_H_
#include <iostream>
#include <memory>
#include <vector>
using namespace std;
namespace ds {

enum TRAVERSAL_ORDER {
  IN_ORDER = 0,
  PRE_ORDER = 1,
  POST_ORDER = 2
};

template<class U>
struct BT_Node {
  U data;
  std::shared_ptr<BT_Node<U>> left;
  std::shared_ptr<BT_Node<U>> right;
  BT_Node(U data) : data(data), left(nullptr), right(nullptr) {}
};

template<class U>
class Binary_Tree {
 public:

  void create_complete(std::vector<U> data) {
    root = insert_complete(root, data, 0);
  }

  std::shared_ptr<BT_Node<U>> insert_complete(std::shared_ptr<BT_Node<U>> node, std::vector<U> data, int index) {
    if (index < data.size()) {
      auto temp = std::make_shared<BT_Node<U>>(data[index]);
      node = std::move(temp);
      node->left = std::move(insert_complete(node->left, data, 2 * index + 1));
      node->right = std::move(insert_complete(node->right, data, 2 * index + 2));
    }
    return node;
  }

  int totalNumberOfNodes(std::shared_ptr<BT_Node<U>> node) {
    if (node == NULL) {
      return 0;
    }
    return 1 + totalNumberOfNodes(node->left) + totalNumberOfNodes(node->right);
  }

  int totalNumberOfNodes() {
    return totalNumberOfNodes(root);
  }

  bool is_complete(std::shared_ptr<BT_Node<U>> node, int countOfNodes, int index) {
    if (node.get() == nullptr)
      return true;
    else if (index >= countOfNodes)
      return false;
    else
      return ((is_complete(node->left, countOfNodes, 2 * index + 1))
          && (is_complete(node->right, countOfNodes, 2 * index + 2)));
  }

  bool is_complete() {
    bool s = is_complete(root, totalNumberOfNodes(root), 0);
    return s;
  }

  void create(U data) {
    auto temp = std::make_shared<BT_Node<U>>(data);
    if (!root) {
      root = std::move(temp);
    } else {
      insert(root, temp);
    }
  }

  void insert(std::shared_ptr<BT_Node<U>> node, std::shared_ptr<BT_Node<U>> data_node) {
    if (node->left == nullptr) {
      node->left = std::move(data_node);
    } else if (node->right == nullptr) {
      node->right = std::move(data_node);
    } else if (node->left != nullptr) {
      insert(node->left, std::move(data_node));
    } else
      insert(node->right, std::move(data_node));
  }

  void insert(U data) {
    auto temp = std::make_shared<BT_Node<U>>(data);
    insert(root, temp);
  }

  void inorder(std::shared_ptr<BT_Node<U>> node) {
    if (node == NULL) {
      return;
    }
    inorder(node->left);
    cout << node.get()->data << " ";
    inorder(node.get()->right);
  }

  void postorder(std::shared_ptr<BT_Node<U>> node) {
    if (node.get() == nullptr) {
      return;
    }
    postorder(node->left);
    postorder(node->right);
    cout << node->data << " ";
  }

  void preorder(std::shared_ptr<BT_Node<U>> node) {
    if (node.get() == nullptr) {
      return;
    }
    cout << node->data << " ";
    preorder(node->left);
    preorder(node->right);
  }

  void print(TRAVERSAL_ORDER to) {
    cout << "Binary tree: ";
    switch (to) {
      case PRE_ORDER:preorder(root);
        break;
      case POST_ORDER:postorder(root);
        break;
      case IN_ORDER:inorder(root);
        break;
      default:break;
    }
    cout << "\n";
  }

  bool is_full(std::shared_ptr<BT_Node<U>> node) {
    if (node->left == NULL && node->right == NULL) {
      return true;
    } else if (node->left != NULL && node->right != NULL) {
      return (is_full(node->left) && is_full(node->right));
    } else return false;
  }

  bool is_full() {
    if (root == NULL) {
      return true;
    } else
      return is_full(root);
  }

  int max_depth_perfect(std::shared_ptr<BT_Node<U>> node) {
    if (node.get() == nullptr) {
      return 0;
    }
    int max = 0;

    int left_depth = max_depth(node->left);
    max += left_depth + 1;

    return max;
  }

  int max_depth(std::shared_ptr<BT_Node<U>> node) {
    if (node.get() == nullptr) {
      return 0;
    }
    int left_depth = max_depth(node->left);
    int right_depth = max_depth(node->right);
    int max = std::max(left_depth, right_depth) + 1;
    return max;
  }

  bool is_perfect(std::shared_ptr<BT_Node<U>> node, int depth, int level) {
    if (node.get() == nullptr) {
      cout << " nul ptr " << " \n";
      return true;
    } else if (node->left == NULL && node->right == NULL) {
      cout << " depth " << depth << " level " << level << " \n";
      return (depth == level + 1);
    } else if (node->right == NULL || node->left == NULL) {
      return false;
    } else if (node->left != NULL && node->right != NULL) {
      return is_perfect(node->left, depth, level + 1) && is_perfect(node->right, depth, level + 1);
    }
  }

  bool is_perfect() {
    if (!root)
      return true;
    else {
      cout << max_depth_perfect(root) << "\n";
      return is_perfect(root, max_depth_perfect(root), 0);
    }
  }

  auto get_root() {
    return root;
  }

  void set_root(std::shared_ptr<BT_Node<U>> node) {
    root = std::move(node);
  }

 private:
  std::shared_ptr<BT_Node<U>> root;
}; // class Binary_Tree
} // namespace ds
#endif MLTEMPLATE_LIBRARY_INCLUDE_DS_BT_BT_H_
