// COPYRIGHT 2020 DLR
#ifndef MLTEMPLATE_LIBRARY_INCLUDE_DS_SEARCH_SEARCH_H_
#define MLTEMPLATE_LIBRARY_INCLUDE_DS_SEARCH_SEARCH_H_

#include <iostream>
#include <memory>
#include <vector>
#include <algorithm>
using namespace std;
namespace algorithm {
enum SEARCH_ALGORITHM {
  LINEAR_SEARCH = 0,
  BINARY_SEARCH = 1
};

template<class U>
class Search {
 public:
  Search(SEARCH_ALGORITHM alg) : m_algorithm(alg) {}

  bool search(std::vector<U> list, U data) {
    bool found = false;
    switch (m_algorithm) {
      case LINEAR_SEARCH:break;
      case BINARY_SEARCH:found = binary_search(list, data);
        break;
      default:break;
    }
    return found;
  }

 private:

  bool linear_search(std::vector<U> list, U data) {
    for (auto i:list) {
      if (i == data)
        return true;
    }
    return false;
  }

  bool binary_search(std::vector<U> list, U data) {
    std::sort(list.begin(), list.end());
    int low = 0;
    int high = list.size() - 1;

    while (low <= high) {
      int middle = (low + high) / 2;
      U temp = list[middle];
      if (list[middle] == data) {
        return true;
      } else if (data < list[middle]) {
        high = middle - 1;
      } else if (data > list[middle]) {
        low = middle + 1;
      }
    }
    return false;
  }

  bool linear_search() {

  }

  SEARCH_ALGORITHM m_algorithm;
};

}

#endif MLTEMPLATE_LIBRARY_INCLUDE_DS_SEARCH_SEARCH_H_
