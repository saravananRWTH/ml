//
// Created by palanisamy on 25.09.20.
//

// COPYRIGHT 2020 Saravanan Palanisamy
#ifndef MLTEMPLATE_LIBRARY_INCLUDE_MATRIX_TRANSPOSE_TRANSPOSE_H_
#define MLTEMPLATE_LIBRARY_INCLUDE_MATRIX_TRANSPOSE_TRANSPOSE_H_
#pragma once
#include <cstdio> // used to access C style IO (e.g. printf)
#include <iostream> // used to access C++ style IO
#include <vector> // used to access C++ vector types
#include <algorithm>
#include <iterator>
#include <type_traits>

#include "../../opencl/Opencl.h"
#include "../../util/utils.h"

#define BLOCK_DIM 16

// max GPU's to manage for multi-GPU parallel compute
const unsigned int MAX_GPU_COUNT = 8;

using namespace std;
using namespace cl;
namespace ML {
class Transpose : public OpenCLFactory {

 public:
  Transpose() {
    this->setHostProgram();
  }

  ~Transpose() {
  }

  MLResult<std::vector<float>> calculate_transpose(std::vector<std::vector<float>> &matrix) {
    MLResult<std::vector<float>> result;
    const int dim = matrix[0].size() * matrix.size();
    std::size_t szGlobalWorkSize[2];
    std::size_t szLocalWorkSize[2];
    try {
      float *mat = convert_2d_to_1d(matrix);
      float mat1[matrix.size() * matrix[0].size()];
      for (int i = 0; i < matrix.size() * matrix[0].size(); i++)
        cout << mat[i] << endl;

      auto size_x = matrix[0].size();
      auto size_y = matrix.size();
      int ciDeviceCount = this->getDeviceCount();
      std::size_t sizePerGPU = shrRoundUp(BLOCK_DIM, (size_x + ciDeviceCount - 1) / ciDeviceCount);
      // size of memory required to store the matrix
      const std::size_t mem_size = sizeof(float) * size_x * size_y;

      cl::Buffer kernel_matrix[MAX_GPU_COUNT];
      cl::Buffer kernel_transpose[MAX_GPU_COUNT];

      for (unsigned int i = 0; i < ciDeviceCount; ++i) {
        cout << "creating program kernel " << endl;
        kernel_matrix[i] = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                                      mem_size, mat1);
        // allocate device memory and copy host to device memory
        kernel_transpose[i] = cl::Buffer(context, CL_MEM_WRITE_ONLY,
                                         sizePerGPU * size_y * sizeof(float), mat1);

        // set the args values for the naive kernel
        std::size_t offset = i * sizePerGPU;

        this->createProgramKernel("Transpose.cl", "Transpose");

        kernel.setArg(0, kernel_transpose);
        kernel.setArg(1, kernel_matrix);
        kernel.setArg(2, offset);
        kernel.setArg(3, size_x);
        kernel.setArg(4, size_y);
        kernel.setArg(5, (BLOCK_DIM + 1) * BLOCK_DIM * sizeof(float), 0);
        kernelList.push_back(std::move(kernel));
      }
      // set up execution configuration
      szLocalWorkSize[0] = BLOCK_DIM;
      szLocalWorkSize[1] = BLOCK_DIM;
      szGlobalWorkSize[0] = sizePerGPU;
      szGlobalWorkSize[1] = shrRoundUp(BLOCK_DIM, size_y);

      for (unsigned int k = 0; k < ciDeviceCount; ++k) {
        // Do the work
        err = queueList[k].enqueueNDRangeKernel(
            kernelList[k], cl::NullRange,
            cl::NDRange(sizePerGPU, shrRoundUp(BLOCK_DIM, size_y)),
            cl::NDRange(BLOCK_DIM, BLOCK_DIM)
        );
      }

      for (unsigned int k = 0; k < ciDeviceCount; ++k) {
        std::size_t offset = k * sizePerGPU;
/*        std::size_t size = MIN(size_x - k * sizePerGPU, sizePerGPU);
        // Map cBuffer to host pointer. This enforces a sync with
// the host backing space; remember we chose a GPU device.
        queueList[k].enqueueMapBuffer(kernel_transpose[k], CL_TRUE, CL_MAP_READ, 0,
                                      size * size_y * sizeof(float));
   */   }

    }
    catch (cl::Error &e) {}

    return result;
  }

};  // class Transpose
}  // namespace ML

#endif //MLTEMPLATE_LIBRARY_INCLUDE_MATRIX_TRANSPOSE_TRANSPOSE_H_
