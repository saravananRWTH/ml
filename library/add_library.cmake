macro(addLibrary LIB_NAME DIRECTORY_NAME)

    find_package(OpenCV REQUIRED)
    include_directories(${OpenCV_INCLUDE_DIRS})

    file(GLOB_RECURSE LIB_FILES ${DIRECTORY_NAME}/*)
    file(GLOB_RECURSE CL_FILES ${DIRECTORY_NAME}/*.cl)
    list(REMOVE_ITEM LIB_FILES ${DIRECTORY_NAME}/CMakeLists.txt)
    add_library(${LIB_NAME})
    target_sources(${LIB_NAME} PUBLIC ${LIB_FILES})

    target_include_directories(${LIB_NAME} PUBLIC
            PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/"
            PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include"
            PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include/stats"
            PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include/utils"
            PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include/${LIB_NAME}"
            PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/test"
            )
    target_link_libraries(${LIB_NAME}
            PUBLIC BETTERENUMS::BETTERENUMS
            PUBLIC Boost::boost
            PUBLIC Expected
            PUBLIC OpenCL::OpenCL
            PUBLIC ${OpenCV_LIBS}
            )

    foreach (file ${CL_FILES})
        file(COPY ${file}
                DESTINATION ${CMAKE_BINARY_DIR}/library)
        file(COPY ${file}
                DESTINATION ${CMAKE_BINARY_DIR}/library)
    endforeach ()

endmacro()
