# ./CMakeLists.txt

# to follow the More-Modern-CMake approach >= 3.12 is required
# see:
# https://github.com/Bagira80/More-Modern-CMake/blob/master/example-3.12/CMakeLists.txt
# https://www.youtube.com/watch?v=y7ndUhdQuU8&feature=youtu.be

cmake_minimum_required(VERSION 3.14)

project(MLTemplate
        VERSION 0.1.0
        DESCRIPTION "Machine learning basics with OpenCL ...")


# Create targets for building a shared library libasn1_codec.so.0.2.0
set(CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake" ${CMAKE_MODULE_PATH})

# 19Dec2019 Oh
#           see: https://mropert.github.io/2018/02/02/pic_pie_sanitizers/
#           Don't understand the flags fully => Don't use it!
# Always use '-fPIC'/'-fPIE' option.
####set( CMAKE_POSITION_INDEPENDENT_CODE ON )

find_package(Git QUIET)
if (GIT_FOUND AND EXISTS "${PROJECT_SOURCE_DIR}/.git")
    # Update submodules as needed
    option(GIT_SUBMODULE "Check submodules during build" ON)
    if (GIT_SUBMODULE)
        message(STATUS "Submodule update")
        execute_process(COMMAND ${GIT_EXECUTABLE} submodule update --init --recursive
                WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                RESULT_VARIABLE GIT_SUBMOD_RESULT)
        if (NOT GIT_SUBMOD_RESULT EQUAL "0")
            message(WARNING "git submodule update --init failed with ${GIT_SUBMOD_RESULT}, please checkout submodules")
        endif ()
    endif ()
endif ()

#if (NOT EXISTS "${PROJECT_SOURCE_DIR}/external/caf/caf/CMakeLists.txt")
#    message(WARNING "The submodules were not downloaded! GIT_SUBMODULE was turned off or failed. Please update submodules and try again.")
#endif ()

message(STATUS "Submodule update - done")

# Make building with cmake verbose => use later >make VERBOSE=1
set(CMAKE_VERBOSE_MAKEFILE ON)

# Make external libraries globally available.
add_subdirectory(external)

# Create targets for building the (local) libraries.
add_subdirectory(library)
#add_subdirectory(docs)
SET(LIB_DIR ${CMAKE_CURRENT_SOURCE_DIR}/library INTERNAL)
# does NOT work
####set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)

## define header only includes for all targets
#include_directories (
#		    "${CMAKE_CURRENT_SOURCE_DIR}/external/better-enums/"
#		    "${CMAKE_CURRENT_SOURCE_DIR}/external/expected/"
enable_testing()
add_test(MLTemplateTest library)

#		    "${CMAKE_CURRENT_SOURCE_DIR}/external/hash/"
#		    )
