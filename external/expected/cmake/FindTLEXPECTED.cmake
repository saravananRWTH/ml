#Try to find tl/expected.hpp header
#
# Use this module as follows:
#
#     find_package(TLEXPECTED)
#
# Variables used by this module (they can change the default behaviour and need
# to be set before calling find_package):
#
#  TLEXPECTED_ROOT_DIR  Set this variable to an installation prefix 
#
# Variables defined by this module:
#
#  TLEXPECTED_FOUND              System has CAF headers and library
#  TLEXPECTED_INCLUDE_DIR       List of include paths for all components

# The tl/expected.hpp from https://github.com/TartanLlama/expected.git does not
# provide a cmake config file, e.g. ExpectedConfig.cmake, thus we search for the
# header files directly and are not able to use the find_package routine in the
# config mode.

# try to find information via pkg-config. Currently the expected library is not
# a part of pkg-config.
# include(LibFindMacros)

find_path( TLEXPECTED_INCLUDE_DIR
	   NAMES "tl/expected.hpp"
	   HINTS "expected/tl" "expected" 
	         "expected/include/tl" "include/tl"
	   PATHS "/usr/include"
                 "/usr/local/include"
		 "/opt/local/include"
		 "$ENV{HOME}"
		 "$ENV{HOME}/include"
		 "$ENV{HOME}/src"
		 "$ENV{HOME}/lib"
		 "$ENV{HOME}/bin"
		 "$ENV{HOME}/Applications"
		 "$ENV{HOME}/expected"
		 "$ENV{HOME}/expected/tl"
		 ENV TLEXPECTED_ROOT
		     CPLUS_INCLUDE_PATH
		     C_INCLUDE_PATH
		     SPACE_ROOT 
		     SPACE_LIB 
           DOC "Path to expected.hpp "
	   "(https://github.com/TartanLlama/expected.git)"
           PATH_SUFFIXES "expected/tl" "expected/include/tl" 
	                 "include/tl" "expected")
mark_as_advanced(TLEXPECTED_INCLUDE_DIR)

if( NOT TLEXPECTED_INCLUDE_DIR STREQUAL "TLEXPECTED_INCLUDE_DIR-NOTFOUND")
  #read the content of the file, and create an error if it does not work
  file(READ "${TLEXPECTED_INCLUDE_DIR}/tl/expected.hpp" expected_hpp_content)
  if(NOT expected_hpp_content)
    message(STATUS "Cannot read ${TLEXPECTED_INCLUDE_DIR}/tl/expected.hpp")
    unset(TLEXPECTED_INCLUDE_DIR)
  endif()

  #check if the file contains the expected string "define TL_EXPECTED_HPP"
  string(REGEX MATCH "#define TL_EXPECTED_HPP" 
         TL_EXPECTED_HPP_FOUND ${expected_hpp_content})
  #stop if the file could not be found
  if(NOT TL_EXPECTED_HPP_FOUND)
    message(STATUS "Header Guard not found [${TLEXPECTED_INCLUDE_DIR}/tl/expected.hpp]")
    unset(TLEXPECTED_INCLUDE_DIR)
  endif()

  #go on if the file was found
  if(TL_EXPECTED_HPP_FOUND) 
    #extract the MAJOR Version from the header file
    string(REGEX MATCH "TL_EXPECTED_VERSION_MAJOR ([0-9]*)" _ 
      ${expected_hpp_content})
      #stop if version number cannot be found
      if(NOT ${CMAKE_MATCH_COUNT}) 
        message(STATUS "Cannot read library version "
	               "[${TLEXPECTED_INCLUDE_DIR}/tl/expected.hpp]")
        unset(TLEXPECTED_INCLUDE_DIR)
      endif()
      #store it in a variable
      set(TLEXPECTED_MAJOR_VERSION ${CMAKE_MATCH_1})
      #extract the MINOR Versin from the header file
      string(REGEX MATCH "TL_EXPECTED_VERSION_MINOR ([0-9]*)" _ 
	     ${expected_hpp_content})
      if(NOT ${CMAKE_MATCH_COUNT}) 
        message(STATUS "Cannot read library version "
	               "[${TLEXPECTED_INCLUDE_DIR}/tl/expected.hpp]")
        unset(TLEXPECTED_INCLUDE_DIR)
      endif()
      #store it in a variable
      set(TLEXPECTED_MINOR_VERSION ${CMAKE_MATCH_1})
      #Remove the tl/ from the 
      get_filename_component(EXPECTED_TL_INCLUDE_DIR "${TLEXPECTED_INCLUDE_DIR}/" 
	                     PATH)
  endif()
endif()

#check if everything is correct and set TLEXPECTED_FOUND accordingly
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(TLEXPECTED 
  FOUND_VAR TLEXPECTED_FOUND
  REQUIRED_VARS TLEXPECTED_INCLUDE_DIR
  VERSION_VAR "${TLEXPECTED_MAJOR_VERSION}.${TLEXPECTED_MINOR_VERSION}")
