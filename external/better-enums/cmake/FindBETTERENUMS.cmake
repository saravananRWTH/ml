#Try to find aantron/enum.h header
#
# Use this module as follows:
#
#     find_package(BETTERENUMS)
#
# Variables used by this module (they can change the default behaviour and need
# to be set before calling find_package):
#
#  BETTERENUMS_ROOT_DIR  Set this variable to an installation prefix 
#
# Variables defined by this module:
#
#  BETTERENUMS_FOUND              System has CAF headers and library
#  BETTERENUMS_INCLUDE_DIR        List of include paths for all components

# try to find information via pkg-config. Currently the ASN1-Runtime library 
# from https://github.com/tysonite/asn1-compiler.git does not support this
# and is not a part of pkg-config.
# include(LibFindMacros)

# The asn1runtime from https://github.com/tysonite/asn1-compiler.git does 
# not provide a cmake config file e.g. ASN1RUNTIMEConfig.cmake, thus we
# must search directly for the files and are not able to use the find_package
# routine in the config mode.

find_path( BETTERENUMS_INCLUDE_DIR
           NAMES "enum.h"
           HINTS "better-enums"
           PATHS "/usr/include"
                 "/usr/local/include"
                 "/opt/local/include"
                 "/opt/gds"
                 "/opt/gds/lib"
                 "$ENV{HOME}"
                 "$ENV{HOME}/include"
                 "$ENV{HOME}/src"
                 "$ENV{HOME}/lib"
                 "$ENV{HOME}/bin"
                 "$ENV{HOME}/data"
		 "$ENV{HOME}/DATA"
                 "$ENV{HOME}/asn1-compiler"
                 "$ENV{HOME}/Applications"
		 ENV BETTERENUMS_ROOT
                     CPLUS_INCLUDE_PATH
                     C_INCLUDE_PATH
                     SPACE_ROOT
                     SPACE_LIB
            DOC "Path to enum.h"
           "(See https://github.com/aatron/better-enums.git)"
           PATH_SUFFIXES "better-enums")
                         
mark_as_advanced(BETTERENUMS_INCLUDE_DIR)

#unset the variable if it was not found, this indicates failure to 
#FindPackageHandleStandardArgs
if( BETTERENUMS_INCLUDE_DIR STREQUAL "BETTERENUMS_INCLUDE_DIR-NOTFOUND" )
  unset(BETTERENUMS_INCLUDE_DIR)
endif()

if( ${BETTERENUMS_INCLUDE_DIR} )
  file(READ "${BETTERENUMS_INCLUDE_DIR}/enum.h"
       enum_h_content)
  #abort if content cannot be read
  if(NOT enum_h_content)
	  message(WARNING "Cannot read content of "
		  "${BETTERENUMS_INCLUDE_DIR}/enum.h")
	  unset(BETTERENUMS_INCLUDE_DIR)
  endif()
   #verify some of the file content, unfortunatly, header files contain no vers.
   string(REGEX MATCH "#define BETTER_ENUMS_ENUM_H" MATCH_RESULT
                     ${enum_h_content})
  if(NOT MATCH_RESULT)
	  message(WARNING "Cannot read content of "
		"${BETTERENUMS_INCLUDE_DIR}/BERValueWriter.hh")
	unset(BETTERENUMS_INCLUDE_DIR)
  endif()
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(BETTERENUMS
  FOUND_VAR BETTERENUMS_FOUND
  REQUIRED_VARS BETTERENUMS_INCLUDE_DIR) 

if(BETTERENUMS_FOUND AND NOT TARGET BETTERENUMS::BETTERENUMS)
  #import the library into the global namespace
  add_library(BETTERENUMS::BETTERENUMS UNKNOWN IMPORTED GLOBAL)
  set_target_properties(BETTERENUMS::BETTERENUMS PROPERTIES
	  IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
	  INTERFACE_INCLUDE_DIRECTORIES "${BETTERENUMS_INCLUDE_DIR}")
endif()
